const makeLoaderFinder = require('razzle-dev-utils/makeLoaderFinder');
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');

module.exports = {
  options: {
    enableReactRefresh: true,
  },
  plugins: ['eslint'],
  modifyWebpackConfig({
    env: { target, dev },
    webpackConfig,
    webpackObject,
    options: { razzleOptions, webpackOptions },
    paths,
  }) {

    if(webpackConfig.devServer){
      webpackConfig.devServer.watchOptions.poll = 2000;
      webpackConfig.devServer.watchOptions.aggregateTimeout = 300;
    }

    webpackConfig.module.rules[
      webpackConfig.module.rules.findIndex(makeLoaderFinder('file-loader'))
    ].exclude.push(/\.(svg)$/);

    webpackConfig.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack', 'url-loader'],
    });

    webpackConfig.watchOptions = {
      poll: Number(process.env.POLLING),
    };
    webpackConfig.plugins.push(
      new MonacoWebpackPlugin({
        languages: ['html', 'css', 'javascript', 'typescript'],
      }),
    );
    return webpackConfig;
  },
};