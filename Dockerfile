FROM node:14.15.0-alpine3.12 as base
ENV NODE_OPTIONS=--max_old_space_size=4096
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
COPY patches ./patches
RUN npm run postinstall
COPY . .
# RUN ["npm", "run", "tsc:noemit"]

FROM base as dev
EXPOSE 4201
CMD ["npm", "run", "start"]

FROM base as build
RUN ["npm", "run", "build"]

FROM nginx:1.19.4 as prod-nginx
COPY --from=build /app/build/public /etc/nginx/html
COPY ./nginx.default.conf /etc/nginx/conf.d/default.conf


FROM node:14.16.1 as prod-node
WORKDIR /app
ENV NODE_ENV=production
COPY package.json package-lock.json ./
RUN npm install --only=production
COPY --from=build /app/build /app/build
CMD npm run start:prod