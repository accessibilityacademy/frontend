import { StrictMode } from 'react';
import { hydrate } from 'react-dom';
import { App } from './App';
import { BrowserRouter } from './BrowserRouter';
import { ServiceProvider } from './services/ServiceProvider';
import reportWebVitals from './reportWebVitals';
import { createBrowserContext } from './helpers/useServerEffect';
import { HelmetProvider } from 'react-helmet-async';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';
import { HttpContextProvider } from './services/HttpContext';

const BrowserDataContext = createBrowserContext();
const emotionCache = createCache({ key: 'css' });
emotionCache.compat = true;

hydrate(
    <StrictMode>
        <HelmetProvider>
            <BrowserDataContext>
                <CacheProvider value={emotionCache}>
                    <HttpContextProvider>
                        <BrowserRouter>
                            <ServiceProvider>
                                <App />
                            </ServiceProvider>
                        </BrowserRouter>
                    </HttpContextProvider>
                </CacheProvider>
            </BrowserDataContext>
        </HelmetProvider>
    </StrictMode>,
    document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

if (module.hot) {
    module.hot.accept();
}
