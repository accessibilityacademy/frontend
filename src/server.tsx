import express, { NextFunction, Request, Response } from 'express';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom/server';
import { createServerContext } from './helpers/useServerEffect';
import { App } from './App';
import { ServiceProvider } from './services/ServiceProvider';
import axios from 'axios';
import { Axios } from 'axios-observable';
import { StoreKey, StoreService } from './services/StoreService';
import cookieParser from 'cookie-parser';
import { HelmetProvider, FilledContext as FilledHelmetContext } from 'react-helmet-async';
import createEmotionServer from '@emotion/server/create-instance';
import createCache from '@emotion/cache';
import { CacheProvider } from '@emotion/react';
import { HttpContextData, HttpContextProvider } from './services/HttpContext';
import jsesc from 'jsesc';


type Assets = {
    client: {
        js: string[];
        css: string[];
        map: string[];
    };
};


let assets: Assets;

const syncLoadAssets = () => {
    assets = require(process.env.RAZZLE_ASSETS_MANIFEST!);
};
syncLoadAssets();

const cssLinksFromAssets = (assets: Assets, entrypoint: keyof Assets) => {
    return assets[entrypoint] ? assets[entrypoint].css ?
        assets[entrypoint].css.map(asset =>
            `<link rel="stylesheet" href="${asset}">`,
        ).join('') : '' : '';
};

const jsScriptTagsFromAssets = (assets: Assets, entrypoint: keyof Assets, extra = '') => {
    return assets[entrypoint] ? assets[entrypoint].js ?
        assets[entrypoint].js.map(asset =>
            `<script src="${asset}"${extra}></script>`,
        ).join('') : '' : '';
};


export const renderApp = async (req: Request, res: Response) => {
    // const context: any = {};

    const { ServerDataContext, resolveData } = createServerContext();
    const helmetContext = {};
    const httpContext: HttpContextData = {};

    const emotionCache = createCache({ key: 'css' });
    const { extractCriticalToChunks, constructStyleTagsFromChunks } = createEmotionServer(emotionCache);


    const accessedCookieValues: { [key: string]: string | null } = {};
    const getCookie = (key: string) => {
        accessedCookieValues[key] = req.cookies[key];
        return req.cookies[key] ?? null;
    };

    class ServerAxiosService extends Axios {
        constructor() {
            const axiosInstance = axios.create({
                baseURL: process.env.NODE_ENV === 'production' ?
                    'https://accessibilityacademy.games/api' :
                    'http://host.docker.internal:3000/api',
                headers: {
                    cookie: `.AspNetCore.Cookies=${req.cookies['.AspNetCore.Cookies']}`,
                },
            });

            super(axiosInstance);

            this.interceptors.response.use((resp) => resp, (err) => {
                if(err.response.status === 401) {
                    res.clearCookie(StoreKey.EMAIL);
                    res.clearCookie(StoreKey.AEM);
                    httpContext.redirectLocation = '/';
                }
                return Promise.reject(err);
            });
        }
    }

    const appJSX = (
        <HelmetProvider context={helmetContext}>
            <ServerDataContext>
                <CacheProvider value={emotionCache}>
                    <HttpContextProvider context={httpContext}>
                        <StaticRouter location={req.url}>
                            <ServiceProvider
                                axiosService={new ServerAxiosService()}
                                storeService={new StoreService(getCookie)}
                            >
                                <App />
                            </ServiceProvider>
                        </StaticRouter>
                    </HttpContextProvider>
                </CacheProvider>
            </ServerDataContext>
        </HelmetProvider>
    );

    renderToString(appJSX);

    const data = await resolveData();

    if(httpContext.redirectLocation) { // if unauthenticated
        return { redirect: httpContext.redirectLocation, code: httpContext.statusCode ?? 302 };
    }

    const unstyledHtml = renderToString(appJSX);


    const { helmet } = helmetContext as FilledHelmetContext;
    const { html: styledHtml, styles } = extractCriticalToChunks(unstyledHtml);

    if (httpContext.redirectLocation) {
        return { redirect: httpContext.redirectLocation, code: httpContext.statusCode ?? 302 };
    } else {
        const html =
        // prettier-ignore
        `<!doctype html>
    <html lang="en">
    <head>
        <meta charSet='utf-8' />
        ${helmet.title.toString()}
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#000000" />
        <meta
            name="description"
            content="Learn web accessibility principles in an interactive way!"
        />
        <meta name="google-site-verification" content="Zj20p8W1mxHonr0XuUtQ7DsL1PTZ5oArrZKH5sejy5Q" />
        ${helmet.link.toString()}
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <!--
          manifest.json provides metadata used when your web app is installed on a
          user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/
        -->
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        ${cssLinksFromAssets(assets, 'client')}
        ${data.toHtml()}
        <script>window._accessedCookieValues = ${jsesc(accessedCookieValues, { json: true, isScriptContext: true })};</script>
        ${constructStyleTagsFromChunks({ html: styledHtml, styles })}
    </head>
    <body>
        <div id="root">${styledHtml}</div>
        ${jsScriptTagsFromAssets(assets, 'client', ' defer crossorigin')}
    </body>
    </html>`;

        return { html, code: httpContext.statusCode };
    }
};

const server = express()
    .disable('x-powered-by')
    .use(express.static(process.env.RAZZLE_PUBLIC_DIR!))
    .use(cookieParser())
    .get('/*', async (req: Request, res: Response, next: NextFunction) => {
        // console.time('Rendering');
        try {
            const { html = '', redirect = false, code = 200 } = await renderApp(req, res);
            if(redirect) {
                res.redirect(code, redirect);
            }  else {
                res.status(code).send(html);
            }
        } catch(err) {
            next(err);
        } finally {
            // console.timeEnd('Rendering');
        }

    });

export default server;
