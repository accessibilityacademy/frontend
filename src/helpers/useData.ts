import { Observable } from 'rxjs';
import { useState, useCallback, useRef } from 'react';
import { HttpTaskState, AsyncHttpTask, StateType, isLoading } from './AsyncHttpTask';
import { useAutoCleanSubscriptions } from './useAutoCleanSubscriptions';
import { useServerEffect } from './useServerEffect';
import { tap } from 'rxjs/operators';

export const useData = <T>(key: string, httpRequest: Observable<T>, deps: unknown[] = []): [HttpTaskState<T>, () => void] => {
    const [state, setState] = useState<HttpTaskState<T>>({ type: StateType.LOADING });

    const subs = useAutoCleanSubscriptions();
    const isDisplayingServerFetchedQuestion = useRef(true);

    const [serverEffectResult] = useServerEffect(key, () => {
        if(!isLoading(state) || isDisplayingServerFetchedQuestion.current) {
            setState({ type: StateType.LOADING });
        }
        isDisplayingServerFetchedQuestion.current = false;
        let task = AsyncHttpTask(httpRequest);
        const promise = new Promise<HttpTaskState<T>>((resolve, reject) => {
            task = task.pipe(tap((taskState) => {
                if(!isLoading(taskState)) {
                    resolve(taskState);
                }
            }));
        });
        const subscription = task.subscribe(setState);
        return {
            promise,
            cleanup: () => {
                subscription.unsubscribe();
            },
        };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, deps);

    const retry = useCallback(() => {
        isDisplayingServerFetchedQuestion.current = false;
        setState({ type: StateType.LOADING });
        subs.push(AsyncHttpTask(httpRequest).subscribe(setState));
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [subs, ...deps]);

    const returnedState = isDisplayingServerFetchedQuestion.current ? serverEffectResult ?? state : state;

    return [returnedState, retry];
};