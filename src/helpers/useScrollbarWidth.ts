import { useEffect, useState } from 'react';

export const useScrollbarDimensions = (ref: HTMLElement) => {
    const [width, setWidth] = useState<number>(ref.offsetWidth - ref.clientWidth);
    const [height, setHeight] = useState<number>(ref.offsetHeight - ref.clientHeight);

    useEffect(() => {
        const listener = () => {
            setWidth(ref.offsetWidth - ref.clientWidth);
            setHeight(ref.offsetHeight - ref.clientHeight);
        };

        ref.addEventListener('resize', listener, true);

        return () => {
            ref.removeEventListener('resize', listener, true);
        };
    }, [ref]);

    return {
        width,
        height,
    };
};