import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AxiosError } from 'axios';

export enum StateType {
    LOADING,
    SUCCESS,
    ERROR,
}

type State<T = unknown, E = unknown, K = unknown> = LoadingState | SuccessState<T> | ErrorState<E, K>;

export interface LoadingState {
    type: StateType.LOADING;
    percentage?: number;
}

export interface SuccessState<T> {
    type: StateType.SUCCESS;
    data: T;
}

export interface ErrorState<E, K> {
    type: StateType.ERROR;
    error: E;
    errorType: K;
}

export enum HttpErrorType {
    TIMEOUT,
    NETWORK_OUTAGE,
    SERVER_EXCEPTION,
    CLIENT_EXCEPTION,
}

export type HttpTaskState<Result> = State<Result, AxiosError, HttpErrorType>;

export const AsyncHttpTask = <T>(task: Observable<T>): Observable<HttpTaskState<T>> => {
    return task.pipe(
        map(resp => {
            const successState: HttpTaskState<T> = { type: StateType.SUCCESS, data: resp };
            return successState;
        }),
        catchError((err: AxiosError) => {
            let errState: HttpTaskState<T>;
            if(!err.response) {
                errState = {
                    type: StateType.ERROR,
                    errorType: err.code === 'ECONNABORTED' ? HttpErrorType.TIMEOUT : HttpErrorType.NETWORK_OUTAGE,
                    error: err,
                };
            } else {
                errState = {
                    type: StateType.ERROR,
                    errorType: err.response.status < 500 ? HttpErrorType.CLIENT_EXCEPTION : HttpErrorType.SERVER_EXCEPTION,
                    error: err,
                };
            }
            return of(errState);
        }),
    );
};

export const isLoading = (state?: State | null): state is LoadingState =>  {
    return state?.type === StateType.LOADING;
};

export const isErrored = <T, E, K>(state?: State<T, E, K> | null): state is ErrorState<E, K> =>  {
    return state?.type === StateType.ERROR;
};

export const isLoaded = <T>(state?: State<T> | null): state is SuccessState<T> =>  {
    return state?.type === StateType.SUCCESS;
};
