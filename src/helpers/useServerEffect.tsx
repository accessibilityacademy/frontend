// Original idea from https://github.com/kmoskwiak/useSSE

import jsesc from 'jsesc';
import { useContext, useState, useEffect, DependencyList, PropsWithChildren, createContext } from 'react';

declare global {
    interface Window {
        _initialDataContext: {};
    }
}

interface IInternalContext {
    effects: {
        promise: Promise<unknown>;
        key: string;
        cancel: Function;
    }[];
    resolved: boolean;
}
interface IDataContext {
    [k: string]: { data?: unknown; error?: unknown };
}

export const InternalContext = createContext<IInternalContext>({
    effects: [],
    resolved: false,
});
export const DataContext = createContext<IDataContext>({});

export function useServerEffect<T>(
    key: string,
    effect: () => Promise<T> | { cleanup: () => void; promise: Promise<T> },
    dependencies?: DependencyList,
): T[] {
    const internalContext: IInternalContext = useContext(InternalContext);

    const ctx: IDataContext = useContext(DataContext);

    const [data, setData] = useState(ctx[key]?.data || null);
    const [error, setError] = useState(ctx[key]?.error || null);
    if (!internalContext.resolved && !internalContext.effects.some(x => x.key === key)) {
        let cancel = Function.prototype;

        const effectPr = new Promise((resolve) => {
            cancel = () => {
                if (!ctx[key]) {
                    ctx[key] = { error: { message: 'timeout' } };
                }
                resolve(key);
            };
            const executingEffect = effect();
            let promise: Promise<unknown>;
            if(executingEffect instanceof Promise) {
                promise = executingEffect;
            } else {
                promise = executingEffect.promise;
            }
            promise.then(res => {
                ctx[key] = { data: res };
                resolve(key);
            }).catch(err => {
                ctx[key] = { error: err };
                resolve(key);
            }).finally(() => {
                if(!(executingEffect instanceof Promise)) {
                    executingEffect.cleanup();
                }
            });
        });

        internalContext.effects.push({
            key,
            promise: effectPr,
            cancel: cancel,
        });
    }

    useEffect(() => {
        if (internalContext.resolved && !ctx[key]) {
            const executingEffect = effect();
            let promise: Promise<unknown>;
            if(executingEffect instanceof Promise) {
                promise = executingEffect;
            } else {
                promise = executingEffect.promise;
            }
            promise
                .then((res) => {
                    setData(res);
                })
                .catch((error) => {
                    setError(error);
                });
            if(!(executingEffect instanceof Promise)) {
                return executingEffect.cleanup;
            }
        }
        delete ctx[key];
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, dependencies);

    return [data as T, error as T];
}

export const createBrowserContext = () => {
    const initial: {} = window._initialDataContext ?? {};
    const internalContextValue: IInternalContext = {
        resolved: true,
        effects: [],
    };
    function BrowserDataContext<T>(props: PropsWithChildren<T>) {
        return (
            <InternalContext.Provider value={internalContextValue}>
                <DataContext.Provider value={initial}>
                    {props.children}
                </DataContext.Provider>
            </InternalContext.Provider>
        );
    }

    return BrowserDataContext;
};

const wait = (time: number) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject({ error: 'timeout' });
        }, time);
    });
};

export const createServerContext = () => {
    const ctx: IDataContext = {};
    const internalContextValue: IInternalContext = {
        resolved: false,
        effects: [],
    };
    function ServerDataContext<T>(props: PropsWithChildren<T>) {
        return (
            <InternalContext.Provider value={internalContextValue}>
                <DataContext.Provider value={ctx}>
                    {props.children}
                </DataContext.Provider>
            </InternalContext.Provider>
        );
    }
    const resolveData = async (timeout?: number) => {
        const effects = internalContextValue.effects.map((item) => item.promise);

        if (timeout) {
            const timeOutPr = wait(timeout);

            await Promise.all(
                internalContextValue.effects.map(async (effect, index) => {
                    try {
                        return Promise.race([effect.promise, timeOutPr]);
                    } catch (e) {
                        return effect.cancel();
                    }
                }),
            );
        } else {
            await Promise.all(effects);
        }

        internalContextValue.resolved = true;

        return {
            data: ctx,
            toJSON: function () {
                return this.data;
            },
            toHtml: function () {
                return `<script>window._initialDataContext = ${jsesc(this, { json: true, isScriptContext: true })};</script>`;
            },
        };
    };
    return {
        ServerDataContext,
        resolveData,
    };
};



