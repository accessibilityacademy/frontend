import { useRef, useEffect } from 'react';
import { Subscription } from 'rxjs';

export function useAutoCleanSubscriptions(): { push: (subscription: Subscription) => void; cancel: () => void } {
    const subscriptions = useRef<Subscription[]>([]).current;

    useEffect(() => {
        return () => {
            subscriptions.forEach((sub) => sub.unsubscribe());
        };
    }, [subscriptions]);

    const operations = useRef({
        push: (subscription: Subscription) => {
            subscriptions.push(subscription);
            subscription.add(() => {
                const i = subscriptions.indexOf(subscription);
                if(i !== -1) {
                    subscriptions.splice(i, 1);
                }
            });
        },
        cancel: () => {
            subscriptions.forEach((sub) => sub.unsubscribe());
        },
    });

    return operations.current;
}
