import axios from 'axios';
import { Axios } from 'axios-observable';
import { History } from 'history';
import { StoreKey, StoreService } from './StoreService';

export class AxiosService extends Axios {

    constructor(store: StoreService, history: History) {
        const axiosInstance = axios.create({
            baseURL: '/api',
            // timeout: 1,
        });

        super(axiosInstance);

        this.interceptors.response.use(res => res, err => {
            if(err.response.status === 401) {
                store.removeValue(StoreKey.EMAIL);
                store.removeValue(StoreKey.AEM);
                history.push('/');
            }
            return Promise.reject(err);
        });
    }

}
