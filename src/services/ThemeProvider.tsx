import { createContext, FunctionComponent, useContext, useRef, useState } from 'react';
import { ThemeProvider as MaterailUIThemeProvider, createTheme, darkScrollbar, Theme } from '@material-ui/core';
import { assertNever } from '../helpers/assertNever';
import { useStore } from './ServiceProvider';
import { StoreKey } from './StoreService';

export enum ThemeType {
    Light = 'light',
    Dark = 'dark'
};

// eslint-disable-next-line @typescript-eslint/no-extra-parens
const ThemeContext = createContext<{theme: Theme; themeType: ThemeType; setThemeType: (theme: ThemeType) => void}>(null!);

export const useThemeService = () => {
    return useContext(ThemeContext);
};

export const ThemeProvider: FunctionComponent = (props) => {

    const store = useStore();

    const [themeType, setThemeType] = useState<ThemeType>(ThemeType.Dark);

    const setThemeFromUser = (theme: ThemeType) => {
        setThemeType(theme);
        store.storeValue(StoreKey.THEME, theme, true);
    };

    const isFirstMount = useRef<boolean>(true);

    if(isFirstMount.current && store.getValue(StoreKey.THEME, 'string')) {
        setThemeType((store.getValue(StoreKey.THEME, 'string') as ThemeType | null) ?? ThemeType.Dark);
        isFirstMount.current = false;
    }

    const lightTheme = createTheme({
        palette: {
            mode: 'light',
            primary: {
                main: '#655893',
            },
            secondary: {
                main: '#655893',
            },
            background: {
                default: '#f7f7f7',
            },
        },
        typography: {
            button: {
                textTransform: 'none',
            },
        },
    });

    const darkTheme = createTheme({
        palette: {
            mode: 'dark',
            primary: {
                main: '#9893F5', // 78A0FF
            },
            secondary: {
                main: '#9893F5',
            },
            background: {
                paper: '#1E1E1E',
                default: '#303030',
            },
            text: {
                primary: '#DDD',
            },
        },
        typography: {
            button: {
                textTransform: 'none',
            },
        },
        components: {
            MuiCssBaseline: {
                styleOverrides: {
                    body: darkScrollbar(),
                },
            },
        },
    });

    let theme: Theme = lightTheme;

    if (themeType === ThemeType.Light) {
        theme = lightTheme;
    } else if (themeType === ThemeType.Dark) {
        theme = darkTheme;
    } else {
        assertNever(themeType);
    }

    return (
        <ThemeContext.Provider value={{
            theme,
            themeType,
            setThemeType: setThemeFromUser,
        }}>
            <MaterailUIThemeProvider theme={theme}>
                { props.children }
            </MaterailUIThemeProvider>
        </ThemeContext.Provider>
    );
};
