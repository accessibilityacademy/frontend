import { assertNever } from '../helpers/assertNever';

declare global {
    interface Window {
        _accessedCookieValues: { [key: string]: string | null };
    }
}


export enum StoreKey {
    THEME = 'theme',
    EXERCISE_RESIZER = 'exercise_resizer',
    EDITOR_PREVIEW_RESIZER = 'editor_preview-resizer',
    BIG_FONT_SIZE = 'big_font_size',
    COLOR_FILTER = 'color_filter',
    NEW_LAYOUT = 'new_layout',
    MOBILE_LANDSCAPE_KEYBOARD = 'mobile_landscape_keyboard',
    COMPLETED_EXERCISES = 'completed_exercises',
    CONTINUE_FROM = 'continue_from',
    EMAIL = 'email',
    AEM = 'aem',
}

export class StoreService {

    constructor(private getCookie: (key: StoreKey) => string | null) { }

    storeValue(key: StoreKey, value: boolean, availableOnServer?: boolean): void;
    storeValue(key: StoreKey, value: number, availableOnServer?: boolean): void;
    storeValue(key: StoreKey, value: string, availableOnServer?: boolean): void;

    storeValue(key: StoreKey, value: boolean | number | string, availableOnServer = false): void {
        if(availableOnServer) {
            if(typeof window !== 'undefined') {
                delete window._accessedCookieValues[key];
            }
            document.cookie = `${key}=${value}; max-age=2147483647; samesite=Strict; path=/`;
        } else {
            localStorage.setItem(key, String(value));
        }
    }

    getValue(key: StoreKey, type: 'boolean'): boolean | null;
    getValue(key: StoreKey, type: 'number'): number | null;
    getValue(key: StoreKey, type: 'string'): string | null;

    getValue(key: StoreKey, type: 'boolean' | 'number' | 'string'): boolean | number | string | null {
        let rawValue: string | null;
        if(typeof window !== 'undefined' && window._accessedCookieValues[key] !== undefined) { // even if it's null we care about it
            rawValue = window._accessedCookieValues[key];
        } else {
            rawValue = this.getCookie(key);
        }

        if(!rawValue && typeof localStorage !== 'undefined') {
            rawValue = localStorage.getItem(key);
        }

        if(!rawValue) {
            return null;
        }
        switch(type) {
            case 'boolean':
                return rawValue === 'true';
            case 'number':
                return Number(rawValue);
            case 'string':
                return rawValue;
            default:
                assertNever(type);
        }
        return ''; // Make TS happy
    }

    removeValue(key: StoreKey) {
        document.cookie = `${encodeURIComponent(key)}=; expires=Thu, 01 Jan 1970 00:00:00 GMT; samesite=Strict`;
        if(typeof window !== 'undefined') {
            delete window._accessedCookieValues[key];
        }
        localStorage.removeItem(key);
    }
}