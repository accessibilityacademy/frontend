import { map } from 'rxjs/operators';
import { AxiosService } from './AxiosService';

export interface ExerciseDetails {
    id: number;
    body: string;
    code: string;
    hiddenCode: string;
    title: string;
    slug: string;
    learningMaterial: string;
    requirements: {
        id: number;
        body: string;
    }[];
}

export interface Exercise {
    id: number;
    title: string;
    slug: string;
}

export interface WalkthroughStep {
    id: number;
    title: string;
    step: string;
}

export interface AnswerResult {
    requirementResults: {
        requirementId: number;
        correct: boolean;
    }[];
}



export interface Feedback {
    type: 'false-negative-solution' | 'false-positive-solution' | 'other';
    details: string;
}


export class ExerciseService {
    constructor(private axios: AxiosService) { }

    getExercise(exerciseId: number) {
        return this.axios.get<ExerciseDetails>(`/exercises/${exerciseId}`).pipe(map((resp) => {
            return resp.data;
        }));
    }

    getAllExercises() {
        return this.axios.get<Exercise[]>('/exercises').pipe(map((resp) => {
            return resp.data;
        }));
    }

    getExerciseWalkthrough(exerciseId: number) {
        return this.axios.get<WalkthroughStep[]>(`/exercises/${exerciseId}/walkthrough`).pipe(map((resp) => {
            return resp.data;
        }));
    }

    submitAnswer(exerciseId: number, answer: string) {
        return this.axios.post<AnswerResult>(`/exercises/${exerciseId}/answer`, {
            body: answer,
        }).pipe(map((resp) => {
            return resp.data;
        }));
    }

    submitFeedback(exerciseId: number, editorState: string, feedback: Feedback) {
        return this.axios.post<AnswerResult>(`/exercises/${exerciseId}/feedback`, {
            editorState,
            type: feedback.type,
            details: feedback.details,
        }).pipe(map((resp) => {
            return resp.data;
        }));
    }
}