import { map } from 'rxjs/operators';
import { AxiosService } from './AxiosService';

export interface CourseDetails extends Course {
    exercises: number[];
}

export interface Course {
    id: number;
    title: string;
    description?: string;
    progress: number;
    target: number;
}

export class CourseService {
    constructor(private axios: AxiosService) { }

    getCourse(courseId: number) {
        return this.axios.get<CourseDetails>(`/courses/${courseId}`).pipe(map((resp) => {
            return resp.data;
        }));
    }

    getAllCourses() {
        return this.axios.get<Course[]>('/courses').pipe(map((resp) => {
            return resp.data;
        }));
    }
}