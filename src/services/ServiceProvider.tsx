import { createContext, FunctionComponent, useContext } from 'react';
import { useHistory } from '../BrowserRouter';
import { AxiosService } from './AxiosService';
import { CourseService } from './CourseService';
import { ExerciseService } from './ExerciseService';
import { StoreService } from './StoreService';

interface Services {
    exerciseService: ExerciseService;
    courseService: CourseService;
    storeService: StoreService;
    axiosService: AxiosService;
}

const ServiceContext = createContext<Services>(null!);

export const useExerciseService = () => useContext(ServiceContext).exerciseService;
export const useCourseService = () => useContext(ServiceContext).courseService;
export const useStore = () => useContext(ServiceContext).storeService;
export const useAxios = () => useContext(ServiceContext).axiosService;


const getCookie = (key: string): string | null => {
    var name = key + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
};

export const ServiceProvider: FunctionComponent<Partial<Services>> = (props) => {
    const history = useHistory();

    const storeService = props.storeService || new StoreService(getCookie);
    const axiosService = props.axiosService || new AxiosService(storeService, history!);

    const value: Services = {
        axiosService,
        exerciseService: props.exerciseService || new ExerciseService(axiosService),
        courseService: props.courseService || new CourseService(axiosService),
        storeService,
    };

    return (
        <ServiceContext.Provider value={value}>
            {props.children}
        </ServiceContext.Provider>
    );
};
