import { createContext, FunctionComponent, useContext, useEffect } from 'react';
import { useNavigate } from 'react-router';
import { useResolvedPath } from 'react-router-dom';

export interface HttpContextData {
    redirectLocation?: string;
    statusCode?: number;
}

export const HttpContext = createContext<HttpContextData | null>(null);

interface HttpProviderProps {
    children?: React.ReactChild;
    context?: HttpContextData;
};

export const HttpContextProvider: FunctionComponent<HttpProviderProps> = (props) => {
    return (
        <HttpContext.Provider value={props.context ?? {}}>{props.children}</HttpContext.Provider>
    );
};


export interface RedirectProps {
    to: string;
    replace?: boolean;
    permanent?: boolean;
}

export const Redirect: FunctionComponent<RedirectProps> = (props) => {
    const context = useContext(HttpContext);
    const location = useResolvedPath(props.to);

    const navigate = useNavigate();

    useEffect(() => {
        navigate(props.to, { replace: props.replace });
    }, [props.to, props.replace, navigate]);

    if(context) {
        context.redirectLocation = location.pathname;
    }

    if(props.permanent) {
        return <HttpStatus code={301} />;
    }

    return null;
};

interface HttpStatusProps {
    code: number;
}

export const HttpStatus: FunctionComponent<HttpStatusProps> = (props) => {
    const context = useContext(HttpContext);

    if(context) {
        context.statusCode = props.code;
    }

    return null;
};
