import { CssBaseline } from '@material-ui/core';
import { Routes } from './Routes';
import { ThemeProvider } from './services/ThemeProvider';
import { Helmet } from 'react-helmet-async';


export const App = () => {
    return (
        <ThemeProvider>
            <CssBaseline />
            <Helmet>
                <title>Accessibility Academy</title>
            </Helmet>
            <Routes />
        </ThemeProvider>
    );
};
