import { Box, Container, Typography } from '@material-ui/core';
import { AxiosError } from 'axios';
import { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet-async';
import { ErrorState, HttpErrorType } from '../helpers/AsyncHttpTask';
import { HttpStatus } from '../services/HttpContext';

interface CriticalErrorProps {
    error: ErrorState<AxiosError, HttpErrorType>;
}

export const CriticalError: FunctionComponent<CriticalErrorProps> = (props) => {
    if(props.error.errorType === HttpErrorType.SERVER_EXCEPTION) {
        return (
            <>
                <HttpStatus code={500} />
                <Helmet>
                    <title>Service unavailable - Accessibility Academy</title>
                </Helmet>
                <Container>
                    <Box textAlign="center">
                        <Typography variant="h2" component="h2">500</Typography>
                        <Typography variant="h4" component="h1">Service unavailable</Typography>
                        <Typography>
                            Something is wrong and this request cannot be completed. Try again later!
                        </Typography>
                    </Box>
                </Container>
            </>
        );
    }

    if(props.error.errorType === HttpErrorType.TIMEOUT) {
        return (
            <>
                <HttpStatus code={503} />
                <Helmet>
                    <title>Timeout - Accessibility Academy</title>
                </Helmet>
                <Container>
                    <Box textAlign="center">
                        <Typography variant="h2" component="h2">503</Typography>
                        <Typography variant="h4" component="h1">Timeout</Typography>
                        <Typography>
                            Service is unavailable. Either you have poor network connectivity or the server is overloaded. Try again later!
                        </Typography>
                    </Box>
                </Container>
            </>
        );
    }

    if(props.error.errorType === HttpErrorType.NETWORK_OUTAGE) {
        return (
            <>
                <HttpStatus code={500} />
                <Helmet>
                    <title>No connection - Accessibility Academy</title>
                </Helmet>
                <Container>
                    <Box textAlign="center">
                        <Typography variant="h4" component="h1">Oops. You appear to be offline!</Typography>
                        <Typography>
                            It appears that there is no network connectivity. Check your internet connection and try again!
                        </Typography>
                    </Box>
                </Container>
            </>
        );
    }
    // console.warn('Critical error called with client exception!');
    // console.dir(props.error.error);
    return <>Uncaught Error. Please refresh page</>;
};