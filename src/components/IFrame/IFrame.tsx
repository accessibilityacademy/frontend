import { forwardRef, useCallback, useEffect, useImperativeHandle, useRef } from 'react';
import { css } from '@emotion/react';
import { fromEvent, Observable } from 'rxjs';
import { useTheme } from '@material-ui/core';

const useStyles = () => {
    return {
        iFrame: css({
            border: 0,
            width: '1px',
            minWidth: '100%',
            height: '100%',
            minHeight: 0,
        }),
    };
};

export type Deficiency = 'protanopia' | 'deuteranopia' | 'tritanopia' | 'achromatopsia' | 'none';

interface IFrameProps {
    html: string;
    colorblindnessSimulation: Deficiency;
};

export interface IFrameRef {
    reload: () => void;
}

export const IFrame = forwardRef<IFrameRef, IFrameProps>((props, ref) => {

    useImperativeHandle(ref, () => ({
        reload: () => {
            iFrame.current.contentWindow?.postMessage({
                type: 'reload',
            }, '*');
        },
    }));

    const styles = useStyles();
    const theme = useTheme();

    const html = props.html;

    const iFrame = useRef<HTMLIFrameElement>(null!);

    const forwardHtml = useCallback(() => {
        iFrame.current.contentWindow?.postMessage({
            type: 'content',
            message: html,
        }, '*');
    }, [html]);

    const forwardColorFilter = useCallback(() => {
        iFrame.current.contentWindow?.postMessage({
            type: 'color-filter',
            message: props.colorblindnessSimulation,
        }, '*');
    }, [props.colorblindnessSimulation]);

    const forwardTheme = useCallback(() => {
        iFrame.current.contentWindow?.postMessage({
            type: 'theme',
            message: theme.palette.mode,
        }, '*');
    }, [theme]);

    const eventStream = useRef<Observable<Event> | null>(null);

    useEffect(() => {
        eventStream.current = fromEvent(iFrame.current!, 'load');
    }, []);


    const iFrameLoaded = useRef(false);

    useEffect(() => {
        const subscription = eventStream.current!.subscribe(() => {
            if(!iFrameLoaded.current) {
                iFrameLoaded.current = true;
                forwardHtml();
                forwardColorFilter();
                forwardTheme();
                return;
            }
            iFrameLoaded.current = false;
            iFrame.current.src = '/iframe.html';
        });
        return () => subscription?.unsubscribe();
    }, [forwardHtml, forwardColorFilter, forwardTheme]);

    useEffect(() => {
        forwardHtml();
    }, [forwardHtml]);

    useEffect(() => {
        forwardColorFilter();
    }, [forwardColorFilter]);

    useEffect(() => {
        forwardTheme();
    }, [forwardTheme]);

    return (
        <>
            <iframe
                ref={iFrame}
                css={styles.iFrame}
                title="Result output"
                sandbox="allow-scripts allow-forms"
                src="/iframe.html"
            />
        </>
    );
});
