import { css, Global } from '@emotion/react';
import { useTheme } from '@material-ui/core';
import { FunctionComponent, memo, MutableRefObject, useState, useRef, KeyboardEvent } from 'react';
import { useStore } from '../services/ServiceProvider';
import { StoreKey } from '../services/StoreService';

const useStyles = (direction: 'row' | 'column'/*, scrollbarDims: { width: number; height: number }*/) => {
    const theme = useTheme();
    const rowStyles = css({
        cursor: 'ew-resize',
        width: 12,
        height: '100%',
        top: 0,
        right: -12,
    });

    const columnStyles = css({
        cursor: 'ns-resize',
        height: 12,
        width: '100%',
        left: 0,
        bottom: -12,
    });

    return {
        resizer: css({
            pointerEvents: 'auto',
            touchAction: 'none',
            position: 'absolute',
            zIndex: theme.zIndex.mobileStepper - 1,
        }, direction === 'row' ? rowStyles : columnStyles),
    };
};

interface ResizerProps {
    targetRef: MutableRefObject<HTMLElement | null>;
    siblingRef: MutableRefObject<HTMLElement | null>;
    direction: 'row' | 'column';
    storeChangeOnKey: StoreKey;
}

export const Resizer: FunctionComponent<ResizerProps> = memo((props) => {

    const styles = useStyles(props.direction);

    const dimensionPercentageTargetRef = useRef<number>(0);
    const store = useStore();

    const [isPressed, setIsPressed] = useState(false);

    const onStartDragging = () => {
        setIsPressed(true);
        document.addEventListener('touchmove', onDrag, { passive: false });
        document.addEventListener('mousemove', onDrag, { passive: false });
        document.addEventListener('touchend', onStopDragging, true);
        document.addEventListener('mouseup', onStopDragging, true);
    };

    const onStopDragging = () => {
        setIsPressed(false);
        store.storeValue(props.storeChangeOnKey, dimensionPercentageTargetRef.current, true);
        document.removeEventListener('touchmove', onDrag);
        document.removeEventListener('mousemove', onDrag);
        document.removeEventListener('touchend', onStopDragging, true);
        document.removeEventListener('mouseup', onStopDragging, true);
    };

    const resizeBy = (increaseElement: 'target' | 'sibling', increaseBy: number) => {
        const targetCurrentPercentage = parseFloat(getComputedStyle(props.targetRef.current!).flexBasis);

        let targetChangedPercentage = increaseElement === 'target' ?
            targetCurrentPercentage + increaseBy :
            targetCurrentPercentage - increaseBy;

        if(targetChangedPercentage < 2) {
            targetChangedPercentage = 2;
        } else if(targetChangedPercentage > 98) {
            targetChangedPercentage = 98;
        }

        const siblingChangedPercentage = 100 - targetChangedPercentage;

        const maxDimension = props.direction === 'row' ? 'maxWidth' : 'maxHeight';
        props.targetRef.current!.style[maxDimension] = targetChangedPercentage + '%';
        props.siblingRef.current!.style[maxDimension] = siblingChangedPercentage + '%';
        props.targetRef.current!.style.flexBasis = targetChangedPercentage + '%';
        props.siblingRef.current!.style.flexBasis = siblingChangedPercentage + '%';
    };

    const onKeyDown = (e: KeyboardEvent) => {
        if(props.direction === 'row') {
            if(e.key === 'ArrowLeft') {
                resizeBy('sibling', 2);
            } else if(e.key === 'ArrowRight') {
                resizeBy('target', 2);
            }
        } else if(props.direction === 'column') {
            if(e.key === 'ArrowUp') {
                resizeBy('sibling', 2);
            } else if(e.key === 'ArrowDown') {
                resizeBy('target', 2);
            }
        }
    };

    const onDrag = (e: MouseEvent | TouchEvent) => {
        if(props.targetRef.current && props.siblingRef.current && props.targetRef.current.parentNode) {
            const target = props.targetRef.current.getBoundingClientRect();
            const sibling = props.siblingRef.current.getBoundingClientRect();

            const parentEl = props.targetRef.current.parentNode as HTMLElement;

            let dimensionPercentageTarget: number;
            let dimensionPercentageSibling: number;

            const position = {
                x: e instanceof MouseEvent ? e.clientX : e.targetTouches[0].pageX,
                y: e instanceof MouseEvent ? e.clientY : e.targetTouches[0].pageY,
            };

            if(props.direction === 'row') {
                dimensionPercentageTarget = 100 * (position.x - target.left) / parentEl.clientWidth;
                dimensionPercentageSibling = 100 * (sibling.width - position.x + sibling.left) / parentEl.clientWidth;
            } else {
                dimensionPercentageTarget = 100 * (position.y - target.top) / parentEl.clientHeight;
                dimensionPercentageSibling = 100 * (sibling.height - position.y + sibling.top) / parentEl.clientHeight;
            }

            if(dimensionPercentageTarget > 98) {
                dimensionPercentageTarget = 98;
                dimensionPercentageSibling = 2;
            } else if (dimensionPercentageSibling > 98) {
                dimensionPercentageTarget = 2;
                dimensionPercentageSibling = 98;
            }

            const maxDimension = props.direction === 'row' ? 'maxWidth' : 'maxHeight';

            props.targetRef.current.style[maxDimension] = dimensionPercentageTarget + '%';
            props.siblingRef.current.style[maxDimension] = dimensionPercentageSibling + '%';
            props.targetRef.current.style.flexBasis = dimensionPercentageTarget + '%';
            props.siblingRef.current.style.flexBasis = dimensionPercentageSibling  + '%';

            dimensionPercentageTargetRef.current = dimensionPercentageTarget;
        }
        e.preventDefault();
    };

    return (
        <>
            { isPressed ?
                <Global styles={{
                    '*': {
                        cursor: `${props.direction === 'row' ? 'ew-resize' : 'ns-resize'} !important`,
                    },
                    'body': {
                        pointerEvents: 'none', // Prevent hover events on monaco editor
                        userSelect: 'none', // Required for firefox. PreventDefault does not stop highlighting
                    },
                }} /> :
                null
            }
            <div
                tabIndex={0}
                aria-label="resizer"
                css={styles.resizer}
                onMouseDown={onStartDragging}
                onTouchStart={onStartDragging}
                onKeyDown={onKeyDown}
            />
        </>
    );
});