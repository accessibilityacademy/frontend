import { HTMLAttributes, forwardRef } from 'react';
import { getPanelId, getTabId, useTabContext } from '@material-ui/lab';

export interface TabPanelProps extends HTMLAttributes<HTMLDivElement> {
    /**
   * The content of the component.
   */
    children?: React.ReactNode;
    /**
   * The `value` of the corresponding `Tab`. Must use the index of the `Tab` when
   * no `value` was passed to `Tab`.
   */
    value: string;
}

export const TabPanel = forwardRef<HTMLDivElement, TabPanelProps>((props, ref) => {
    const { children, className, value, ...rest } = props;
    const context = useTabContext();
    if (context === null) {
        throw new TypeError('No TabContext provided');
    }
    const id = getPanelId(context, value);
    const tabId = getTabId(context, value);

    return (
        <div
            aria-labelledby={tabId}
            className={className}
            hidden={value !== context.value}
            id={id}
            ref={ref}
            role="tabpanel"
            {...rest}
        >
            {children}
        </div>
    );
});