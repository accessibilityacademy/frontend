import {
    AppBar,
    Box,
    Button,
    Divider,
    Drawer,
    IconButton,
    List,
    ListItem,
    ListItemText,
    Toolbar,
    Typography,
    useScrollTrigger,
    useTheme,
} from '@material-ui/core';
import { FunctionComponent, useState } from 'react';
import { Menu, Close } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { css } from '@emotion/react';


const useStyles = (compact: boolean) => {
    const theme = useTheme();

    return {
        rectButton: css({
            borderRadius: 0,
        }),
        buttonInheritHeight: css({
            '& .MuiButton-label': {
                height: '100%',
            },
        }),
        imgLogoContainer: css({
            height: '100%',
            padding: theme.spacing(1),
            paddingLeft: 0,
        }),
        logoImg: css({
            height: '100%',
        }),
        sticky: css({
            '@media screen and (min-height: 300px)': {
                position: 'sticky',
            },
            position: 'static',
        }),
        navbarMinHeight: css(compact ? {
            minHeight: '40px !important',
        } : {
            minHeight: 56,
            [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
                minHeight: 48,
            },
            [theme.breakpoints.up('sm')]: {
                minHeight: 64,
            },
        }),
        navbarHeight: css(compact ? {
            height: 40,
        } : {
            height: 56,
            [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
                height: 48,
            },
            [theme.breakpoints.up('sm')]: {
                height: 64,
            },
        }),
    };
};

export const Header: FunctionComponent<{ compact?: boolean }> = (props) => {

    const styles = useStyles(props.compact ?? false);

    const [isDrawerOpen, toggleDrawer] = useState(false);

    const isScrolled = useScrollTrigger({ disableHysteresis: true });

    const AppDrawer = (
        <Drawer anchor="left" open={isDrawerOpen} onClose={() => toggleDrawer(false)}>
            <nav>
                <Box mx={2} my={1}>
                    <Button
                        color="inherit"
                        css={styles.rectButton}
                        role={undefined}
                        component={Link}
                        to="/"
                        onClick={() => toggleDrawer(false)}
                    >
                        <Typography variant="h6" component="span">Accessibility Academy</Typography>
                    </Button>
                    &nbsp;
                    <IconButton edge="end" onClick={() => toggleDrawer(false)}>
                        <Close />
                    </IconButton>
                </Box>
                <List disablePadding onClick={() => toggleDrawer(false)}>
                    <Divider />
                    <ListItem button component={Link} to="/principles">
                        <ListItemText primary="Principles" role={undefined} />
                    </ListItem>
                    <Divider />
                    <ListItem button component={Link} to="/browse-exercises">
                        <ListItemText primary="Browse Exercises" role={undefined} />
                    </ListItem>
                    <Divider />
                    <ListItem button component={Link} to="/courses">
                        <ListItemText primary="Browse Courses" role={undefined} />
                    </ListItem>
                    <Divider />
                    <ListItem button component={Link} to="/settings">
                        <ListItemText primary="Settings" role={undefined} />
                    </ListItem>
                    <Divider />
                </List>
            </nav>
        </Drawer>
    );

    return (
        <>
            <AppBar elevation={isScrolled ? 8 : 2} css={styles.sticky} color="default">
                <Toolbar css={styles.navbarMinHeight}>
                    <Box sx={{ display: { md: 'none', sm: 'block' } }}>
                        <IconButton
                            size={props.compact ? 'small' : 'medium'}
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            onClick={() => { toggleDrawer(true); }}
                        >
                            <Menu />
                        </IconButton>
                    </Box>
                    <Button
                        component={Link}
                        to="/"
                        css={[styles.navbarHeight, styles.buttonInheritHeight, styles.rectButton]}
                        color="inherit"
                    >
                        <img css={styles.logoImg} src="/logo2.svg" alt="logo" />
                        &nbsp;
                        <Typography variant="body1" component="span">
                            Accessibility Academy
                        </Typography>
                    </Button>
                    <Box sx={{ display: { xs: 'none', md: 'block' } }}>
                        <Box ml={2}>
                            <Button
                                component={Link}
                                to="/principles"
                                color="inherit"
                                role={undefined}
                            >
                                Principles
                            </Button>
                            <Button
                                component={Link}
                                to="/browse-exercises"
                                color="inherit"
                                role={undefined}
                            >
                                Browse Exercises
                            </Button>
                            <Button
                                component={Link}
                                to="/courses"
                                color="inherit"
                                role={undefined}
                            >
                                Browse Courses
                            </Button>
                            <Button
                                component={Link}
                                to="/settings"
                                color="inherit"
                                role={undefined}
                            >
                                Settings
                            </Button>
                        </Box>
                    </Box>
                </Toolbar>
            </AppBar>
            { AppDrawer }
        </>
    );



    // return (
    //     <>
    //         <AppBar position="sticky" css={theme => ({ marginBottom: theme.spacing(2) })}>
    //             <Toolbar>
    //                 <Typography variant="body1">
    //                     Accessibility Academy
    //                 </Typography>
    //                 <Box sx={{ marginLeft: 2 }}>
    //                     <Button color="inherit">Training</Button>
    //                     <Button color="inherit">Browse exercises</Button>
    //                     <Button color="inherit">Why?</Button>
    //                 </Box>
    //             </Toolbar>
    //         </AppBar>
    //     </>
    // );
};