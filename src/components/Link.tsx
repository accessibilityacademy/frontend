import { FunctionComponent } from 'react';
import { Link as MaterialLink, LinkProps as MaterialLinkProps } from '@material-ui/core';
import { Link as RouterLink, LinkProps as RouterLinkProps } from 'react-router-dom';

export const Link: FunctionComponent<MaterialLinkProps & RouterLinkProps> = (props) => {
    return (
        <MaterialLink component={RouterLink} underline="always" {...props}>
            {props.children}
        </MaterialLink>
    );
};