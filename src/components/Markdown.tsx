import { Box, Link, Paper, Typography, useTheme } from '@material-ui/core';
import ReactMarkdown, { MarkdownToJSX } from 'markdown-to-jsx';
import React, { FunctionComponent, useEffect, useRef } from 'react';

const Code: FunctionComponent<{ className?: string; children: string }> = (props) => {

    const theme = useTheme();
    const themeMode = theme.palette.mode;
    let lang = props.className?.substring(5);
    if(lang === 'js') {
        lang = 'javascript';
    }
    const ref = useRef<HTMLElement>(null!);

    useEffect(() => {
        (async () => {
            ref.current.classList.remove('vs', 'vs-dark');
            const { monaco } = await import('react-monaco-editor');
            await monaco.editor.colorizeElement(ref.current, { theme: themeMode === 'dark' ? 'vs-dark' : 'vs' }).catch(err => { });
        })().catch(err => { });
    }, [props.children, themeMode]);

    if(!lang) {
        return (
            <Paper sx={{ px: '2px' }} component="span" square variant="outlined">
                <Typography variant="body2" component="code" color="primary">{props.children}</Typography>
            </Paper>
        );
    }

    return (
        <Paper
            variant="outlined"
            css={{
                'p + &': {
                    marginTop: theme.spacing(-2),
                },
            }}
            sx={{ padding: 2, marginBottom: 2, overflowX: 'auto', lineHeight: 'normal' }}
        >
            {/* Inner box used for firefox padding bug */}
            <Box sx={{ paddingRight: 2, width: 'fit-content' }}>
                <code
                    css={{ whiteSpace: 'pre', fontFamily: 'Consolas, "Courier New", monospace' }}
                    ref={ref}
                    lang={lang}
                >
                    {props.children}
                </code>
            </Box>
        </Paper>
    );
};

const options: MarkdownToJSX.Options = {
    overrides: {
        h1: {
            component: Typography,
            props: {
                gutterBottom: true,
                variant: 'h5',
            },
        },
        h2: { component: Typography, props: { gutterBottom: true, variant: 'h6' } },
        h3: { component: Typography, props: { gutterBottom: true, variant: 'subtitle1' } },
        h4: {
            component: Typography,
            props: { gutterBottom: true, variant: 'caption', paragraph: true },
        },
        p: {
            component: Typography,
            props: {
                paragraph: true,
                variant: 'body1',
                sx: { '&:last-child': { marginBottom: 0 } },
            },
        },
        a: {
            component: Link,
            props: {
                sx: { overflowWrap: 'anywhere' },
                underline: 'always',
                target: '_blank',
                rel: 'noopener',
            },
        },
        code: { component: Code },
        pre: { component: (props: { children: string }) => <>{props.children}</> }, // Prevent overflowing code blocks
        ul: {
            component: ((props) => (
                <ul
                    css={(theme) => ({
                        'p + &': {
                            marginTop: theme.spacing(-2),
                        },
                    })}
                    {...props}
                />
            )) as FunctionComponent,
        },
    },
    forceBlock: true,
};

export const Markdown: FunctionComponent<{ children: string }> = (props) => {
    return <ReactMarkdown css={{ overflowX: 'auto' }} options={options} {...props} />;
};
