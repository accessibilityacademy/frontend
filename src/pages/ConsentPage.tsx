import {
    Box, Card, CardContent, Checkbox, Container,
    FormControlLabel, TextField, Typography,
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { FormEvent, useState } from 'react';
import { useNavigate } from 'react-router';
import { CriticalError } from '../components/CriticalError';
import { AsyncHttpTask, isLoading, isErrored, HttpTaskState, isLoaded } from '../helpers/AsyncHttpTask';
import { useAxios, useStore } from '../services/ServiceProvider';
import { StoreKey } from '../services/StoreService';

export const ConsentPage = () => {

    const [consent, setConsent] = useState(false);
    const [email, setEmail] = useState('');
    const [AEM, setAEM] = useState('');

    const navigate = useNavigate();
    const store = useStore();


    const [req, setReq] = useState<HttpTaskState<unknown> | null>(null);

    const axios = useAxios();

    const onSubmit = (e: FormEvent<HTMLElement>) => {
        e.preventDefault();
        const submittedEmail = email;
        const submittedAEM = AEM;
        AsyncHttpTask(axios.post('/user', {
            email,
            AEM,
        })).subscribe(state => {
            if(isLoaded(state)) {
                store.storeValue(StoreKey.EMAIL, submittedEmail, true);
                store.storeValue(StoreKey.AEM, submittedAEM, true);
                navigate('/');
            } else {
                setReq(state);
            }
        });
    };

    if(isErrored(req)) {
        return <CriticalError error={req} />;
    }

    return (
        <Container maxWidth="md" css={{ paddingTop: 8 }}>

            <Typography variant="h4" component="h1" textAlign="center" paragraph>
                Φόρμα συναίνεσης για συμμετοχή στην μελέτη
            </Typography>

            <Typography gutterBottom variant="body2">
                <strong>Τίτλος Εργασίας: </strong>
                Εκπαιδευτικό λογισμικό με θέμα την διαδικτυακή προσβασιμότητα
            </Typography>

            <Typography gutterBottom variant="body2">
                <strong>Επιστημονικός Υπεύθυνος: </strong>
                Δρ. Χρήστος Κατσάνος, Επίκουρος Καθηγητής, Τμήμα Πληροφορικής, Αριστοτέλειο Πανεπιστήμιο Θεσσαλονίκης
            </Typography>

            <Typography gutterBottom variant="body2">
                <strong>Φοιτητής: </strong>
                Θεμιστοκλής Χατζηεμμανουήλ
            </Typography>

            <Typography variant="h5" component="h2" textAlign="center" paragraph>
                Σκοπός της μελέτης
            </Typography>

            <Typography paragraph variant="body2">
                Σκοπός της μελέτης είναι η αξιολόγηση της καταλληλότητας του εκπαιδευτικού λογισμικού ως ένα εργαλείο
                εκμάθησης των κανόνων διαδικτυακής προσβασιμότητας
                για σχεδιαστές, προγραμματιστές και αξιολογητές ιστοτόπων.
            </Typography>

            <Typography variant="h5" component="h2" textAlign="center" paragraph>
                Περίληψη της διαδικασίας
            </Typography>

            <Typography paragraph variant="body2">
                Ο συμμετέχων δεδομένου ότι κατέχει ήδη βασικές γνώσεις HTML και προαιρετικά, CSS και ελάχιστη JavaScript,
                καλείται να διορθώσει προβλήματα προσβασιμότητας ώστε να μπει στην διαδικασία να μάθει για συχνά λάθη που
                συμβαίνουν κατά την ανάπτυξη ενός Ιστοτόπου.
            </Typography>

            <Typography variant="h5" component="h2" textAlign="center" paragraph>
                Δεδομένα που συλλέγονται
            </Typography>

            <ul>
                <Typography component="li" variant="body2">Email</Typography>
                <Typography component="li" variant="body2">ΑΕΜ</Typography>
                <Typography component="li" variant="body2">Χρόνος παραμονής στον Ιστότοπο</Typography>
                <Typography component="li" variant="body2">Χρόνος απάντησης σε μια άσκηση</Typography>
                <Typography component="li" variant="body2">
                    Ολόκληρες οι απάντησεις που δίνονται στις ασκήσεις είτε είναι σωστές είτε όχι
                </Typography>
                <Typography component="li" variant="body2">
                    Αν έγινε χρήση του κουμπιού "Show me the answer"
                </Typography>
            </ul>

            <Typography variant="h5" component="h2" textAlign="center" paragraph>
                Δημοσίευση δεδομένων - αποτελεσμάτων
            </Typography>

            <Typography paragraph variant="body2">
                Η συμμετοχή σας στην μελέτη συνεπάγεται ότι συμφωνείτε με τη μελλοντική δημοσίευση των αποτελεσμάτων της,
                με την προϋπόθεση ότι οι πληροφορίες θα είναι ανώνυμες και δε θα αποκαλυφθεί το email που θα συμπληρώσουν
                οι συμμετέχοντες σε οποιονδήποτε δεν αναφέρεται ρητά στην παρούσα φόρμα (Επιστημονικός υπεύθυνος, Φοιτητής).
            </Typography>

            {/* <Typography variant="h5" component="h2" textAlign="center" paragraph>
                Δικαίωμα υπαναχώρησης
            </Typography>

            <Typography paragraph variant="body2">
                Η διαγραφή των δεδομένων δύναται μέχρι και τα μεσάνυχτα της Κυριακής 13 Ιουνίου,
                κατόπιν επικοινωνίας με τον ερευνητή στο email: chatziem@csd.auth.gr <br />
                Μετά την ημερομηνία αυτή θα πραγματοποιηθεί ανωνυμοποίηση των δεδομένων που παρήχθησαν
                κατά την χρήση της εκπαιδευτικής πλατφόρμας, οπότε και δεν θα υπάρχει πλέον η δυνατότητα διαγραφής. <br />
            </Typography> */}


            <Card>
                <CardContent>
                    <form onSubmit={onSubmit}>
                        <Typography variant="h5" component="h2" gutterBottom>Δήλωση συναίνεσης</Typography>
                        <Box display="flex" flexDirection="column" width="fit-content">
                            <TextField
                                size="small"
                                label="Email"
                                autoComplete="email"
                                type="email"
                                required
                                value={email}
                                id="email"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                            <TextField
                                size="small"
                                sx={{ mt: 2 }}
                                label="ΑΕΜ"
                                onChange={(e) => setAEM(e.target.value)}
                                value={AEM}
                                id="aem"
                                helperText="Υποχρεωτικό για φοιτητές του ΑΠΘ"
                            />

                            <FormControlLabel
                                sx={{ my: 1 }}
                                control={
                                    <Checkbox
                                        size="small"
                                        checked={consent}
                                        onChange={(e) => setConsent(e.target.checked)}
                                        color="primary"
                                        required
                                    />
                                }
                                label="Συναινώ να συμμετάσχω στην μελέτη"
                            />
                        </Box>
                        <LoadingButton loading={(req && isLoading(req)) ?? false}  type="submit" variant="outlined">Επόμενο</LoadingButton>
                    </form>
                </CardContent>
            </Card>

            <footer css={{ marginTop: 8 }}>
                <Typography variant="subtitle2" textAlign="center">
                    Θεμιστοκλής Χατζηεμμανουήλ - chatziem@csd.auth.gr
                </Typography>
                <Typography variant="subtitle2" textAlign="center">
                    Τμήμα Πληροφορικής, Αριστοτέλειο Πανεπιστήμιο Θεσσαλονίκης (ΑΠΘ)
                </Typography>
            </footer>
        </Container>

    );
};