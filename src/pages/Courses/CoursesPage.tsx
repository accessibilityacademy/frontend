import { Box, Button, Container, Skeleton } from '@material-ui/core';
import { Helmet } from 'react-helmet-async';
import { Link as RouterLink } from 'react-router-dom';
import { CriticalError } from '../../components/CriticalError';
import { isErrored, isLoading } from '../../helpers/AsyncHttpTask';
import { useData } from '../../helpers/useData';
import { useCourseService } from '../../services/ServiceProvider';


export const CoursesPage = () => {

    const courseService = useCourseService();
    const [courses] = useData('courses', courseService.getAllCourses());

    if(isErrored(courses)) {
        return <CriticalError error={courses} />;
    }

    return (
        <Container>
            <Helmet>
                <title>Courses - Accessibility Academy</title>
            </Helmet>
            {isLoading(courses) ?
                <>
                    <Box mb={1}>
                        <Skeleton variant="rectangular" height={36} />
                    </Box>
                    <Box mb={1}>
                        <Skeleton variant="rectangular" height={36} />
                    </Box>
                    <Box mb={1}>
                        <Skeleton variant="rectangular" height={36} />
                    </Box>
                </> :
                courses.data.map(course => (
                    <Box key={course.id} mb={1}>
                        <Button
                            variant="outlined"
                            css={{
                                '& .MuiButton-label': {
                                    justifyContent: 'space-between',
                                },
                            }}
                            fullWidth
                            component={RouterLink}
                            to={`${course.id}`}
                            role="link"
                        >
                            <span>
                                {course.title}
                            </span>
                            <span>
                                {course.progress}/{course.target}
                            </span>
                        </Button>
                    </Box>
                ))
            }
        </Container>
    );
};