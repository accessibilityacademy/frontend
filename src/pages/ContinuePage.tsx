import { Box, Card, CardContent, Container, TextField, Typography } from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { FormEvent, useState } from 'react';
import { useNavigate } from 'react-router';
import { CriticalError } from '../components/CriticalError';
import { AsyncHttpTask, isLoading, isErrored, HttpTaskState, isLoaded } from '../helpers/AsyncHttpTask';
import { useAxios, useStore } from '../services/ServiceProvider';
import { StoreKey } from '../services/StoreService';

export const ContinuePage = () => {

    const [email, setEmail] = useState('');
    const navigate = useNavigate();
    const store = useStore();

    const [req, setReq] = useState<HttpTaskState<unknown> | null>(null);

    const axios = useAxios();

    const onSubmit = (e: FormEvent<HTMLElement>) => {
        e.preventDefault();
        const submittedEmail = email;
        AsyncHttpTask(axios.get<{ email: string; aem: string }>(`/user?email=${submittedEmail}`)).subscribe(state => {
            if(isLoaded(state)) {
                if(state.data.data) {
                    store.storeValue(StoreKey.EMAIL, state.data.data.email, true);
                    if(state.data.data.aem) {
                        store.storeValue(StoreKey.AEM, state.data.data.aem, true);
                    }
                    navigate('/');
                } else {
                    navigate('/consent');
                }
            } else {
                setReq(state);
            }
        });
    };

    if(isErrored(req)) {
        return <CriticalError error={req} />;
    }


    return (
        <Container maxWidth="md" css={{ paddingTop: 8 }}>

            <Typography variant="h4" component="h1" textAlign="center" paragraph>
                Συνέχιση αξιολόγησης
            </Typography>

            <Card>
                <CardContent>
                    <form onSubmit={onSubmit}>
                        <Box display="flex" flexDirection="column" width="fit-content">
                            <TextField
                                label="Email"
                                autoComplete="email"
                                type="email"
                                required
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                            <Typography sx={{ mt: 1 }}>Το ΑΕΜ δεν χρειάζεται να συμπληρωθεί ξανά</Typography>
                        </Box>
                        <LoadingButton
                            loading={(req && isLoading(req)) ?? false}
                            sx={{ mt: 1 }}
                            type="submit"
                            variant="outlined"
                        >
                            Συνέχιση αξιολόγησης
                        </LoadingButton>
                    </form>
                </CardContent>
            </Card>

            <footer css={{ marginTop: 8 }}>
                <Typography variant="subtitle2" textAlign="center">
                    Θεμιστοκλής Χατζηεμμανουήλ - chatziem@csd.auth.gr
                </Typography>
                <Typography variant="subtitle2" textAlign="center">
                    Τμήμα Πληροφορικής, Αριστοτέλειο Πανεπιστήμιο Θεσσαλονίκης (ΑΠΘ)
                </Typography>
            </footer>
        </Container>

    );
};