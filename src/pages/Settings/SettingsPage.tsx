import { Container, FormControlLabel, Switch, Typography, useTheme } from '@material-ui/core';
import { ChangeEvent, useState } from 'react';
import { ThemeType, useThemeService } from '../../services/ThemeProvider';
import { StoreKey } from '../../services/StoreService';
import { useStore } from '../../services/ServiceProvider';
import { Helmet } from 'react-helmet-async';

export const SettingsPage = () => {

    const themeService = useThemeService();
    const store = useStore();
    const [newLayout, setNewLayout] = useState(store.getValue(StoreKey.NEW_LAYOUT, 'boolean') ?? false);

    const theme = useTheme();

    const setLayout = (e: ChangeEvent<HTMLInputElement>) => {
        setNewLayout(e.target.checked);
        store.storeValue(StoreKey.NEW_LAYOUT, e.target.checked, true);
        store.removeValue(StoreKey.EXERCISE_RESIZER);
        store.removeValue(StoreKey.EDITOR_PREVIEW_RESIZER);
    };

    return (
        <Container maxWidth="lg" css={{ paddingTop: 8 }}>
            <Helmet>
                <title>Principles - Accessibility Academy</title>
            </Helmet>
            <Typography variant="h6" component="h1">
                Settings
            </Typography>
            <FormControlLabel
                control={
                    <Switch
                        checked={newLayout}
                        onChange={setLayout}
                        name="new-layout"
                        color="primary"
                    />
                }
                label="Toggle new layout of exercise page"
            />
            <br />
            <FormControlLabel
                control={
                    <Switch
                        checked={themeService.themeType === ThemeType.Dark}
                        onChange={
                            (e) => themeService.setThemeType(e.target.checked ? ThemeType.Dark : ThemeType.Light)
                        }
                        name="Dark mode"
                    />
                }
                label="Dark mode"
            />

            <Typography>Preview: </Typography>
            <img
                css={{ width: '100%', border: `1px solid ${theme.palette.divider}` }}
                src={`/${theme.palette.mode}-${newLayout ? 'new' : 'old'}-preview.png`}
                alt="exercise page preview"
            />
        </Container>
    );
};