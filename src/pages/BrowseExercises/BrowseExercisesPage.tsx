import { Box, Button, Container, Skeleton } from '@material-ui/core';
import { CheckBox, CheckBoxOutlineBlank } from '@material-ui/icons';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { Link as RouterLink } from 'react-router-dom';
import { CriticalError } from '../../components/CriticalError';
import { isErrored, isLoading } from '../../helpers/AsyncHttpTask';
import { useData } from '../../helpers/useData';
import { useExerciseService, useStore } from '../../services/ServiceProvider';
import { StoreKey } from '../../services/StoreService';

export const BrowseExercisesPage = () => {

    const store = useStore();

    const exerciseService = useExerciseService();
    const [exercises] = useData('exercises', exerciseService.getAllExercises());

    const [completedExercises, setCompletedExercises] = useState<number[] | null>(null);

    useEffect(() => {
        const completedExercisesSerialized = store.getValue(StoreKey.COMPLETED_EXERCISES, 'string');
        if(completedExercisesSerialized) {
            try {
                setCompletedExercises(JSON.parse(completedExercisesSerialized));
            } catch (e) { }
        }
    }, [store]);

    if(isErrored(exercises)) {
        return <CriticalError error={exercises} />;
    }

    return (
        <Container>
            <Helmet>
                <title>Browse Exercises - Accessibility Academy</title>
            </Helmet>
            {isLoading(exercises) ?
                Array(30).fill(null).map((_, i) => (
                    <Box key={i} mb={1}>
                        <Skeleton variant="rectangular" height={36} />
                    </Box>
                )) :
                exercises.data.map(exercise => (
                    <Box key={exercise.id} mb={1}>
                        <Button
                            variant="outlined"
                            css={{
                                '& .MuiButton-label': {
                                    justifyContent: 'space-between',
                                },
                            }}
                            fullWidth
                            component={RouterLink}
                            to={`/exercise/${exercise.id}/${exercise.slug}`}
                            role="link"
                        >
                            <span>
                                {exercise.title}
                            </span>
                            {
                                completedExercises && completedExercises.some(x => x === exercise.id) ?
                                    <CheckBox titleAccess="Completed" /> :
                                    <CheckBoxOutlineBlank titleAccess="Not completed" />
                            }
                        </Button>
                    </Box>
                ))
            }
        </Container>
    );
};