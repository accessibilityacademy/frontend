import { Backdrop, Box, CircularProgress, Container, Typography } from '@material-ui/core';
import { cloneElement, FunctionComponent, FunctionComponentElement, useEffect, useRef } from 'react';
import { Helmet } from 'react-helmet-async';
import { Route, Routes, useParams } from 'react-router-dom';
import { Header } from '../../components/Header';
import { HttpErrorType, isErrored, isLoaded } from '../../helpers/AsyncHttpTask';
import { useData } from '../../helpers/useData';
import { Redirect, RedirectProps } from '../../services/HttpContext';
import { useCourseService } from '../../services/ServiceProvider';
import { NotFoundPage } from '../NotFoundPage';
import { ExercisePage } from './ΕxercisePage';


export const CourseHandler = () => {
    const courseService = useCourseService();
    const params = useParams();
    const [course] = useData('courseExercises', courseService.getCourse(Number(params.id)));

    if(isErrored(course) && course.errorType === HttpErrorType.CLIENT_EXCEPTION) {
        return (
            <>
                <Header />
                <NotFoundPage />
            </>
        ) ;
    }

    let redirect: JSX.Element | null = null;
    if(isLoaded(course)) {
        if(course.data.progress === course.data.target) {
            redirect = <Redirect to={'completed'} replace />;
        } else {
            redirect = <Redirect to={`exercise/${course.data.exercises[course.data.progress]}`} replace />;
        }
    }


    return (
        <>
            <Routes>
                <Route path="*" element={
                    <>
                        {redirect}
                        <Backdrop css={(theme) => ({
                            color: theme.palette.mode === 'light' ? theme.palette.primary.dark : undefined,
                        })} open>
                            <CircularProgress color="inherit" />
                        </Backdrop>
                    </>
                } />
                <Route
                    path="/exercise/:id/*"
                    element={
                        !isLoaded(course) ?
                            <Backdrop css={(theme) => ({
                                color: theme.palette.mode === 'light' ? theme.palette.primary.dark : undefined,
                            })} open>
                                <CircularProgress color="inherit" />
                            </Backdrop> :
                            <ExercisePageWrapper
                                redirect={redirect}
                                nextExerciseId={course.data.exercises[course.data.progress]}
                            >
                                <ExercisePage exercises={course.data.exercises} courseId={Number(params.id)} />
                            </ExercisePageWrapper>
                    }
                />
                <Route
                    path="/completed"
                    element={
                        <>
                            <Header />
                            <main css={{ marginTop: 8 }}>
                                <Helmet>
                                    <title>Congratulations - Accessibility Academy</title>
                                </Helmet>
                                <Container maxWidth="md">
                                    <Box textAlign="center">
                                        <Typography
                                            variant="h5"
                                            component="h1"
                                            gutterBottom
                                        >
                                            Συγχαρητήρια
                                        </Typography>
                                        <Typography textAlign="left" paragraph>
                                            Ολοκληρώσατε με επιτυχία τις υποχρεωτικές ασκήσεις για την συμμετοχή σας στην μελέτη.
                                        </Typography>
                                        <Typography textAlign="left" paragraph>
                                            Μπορείτε να επιστρέψετε στην αρχική σελίδα και να συνεχίσετε με το δεύτερο quiz γνώσεων
                                            και το ερωτηματολόγιο προκειμένου να ολοκληρωθεί η συμμετοχή σας.
                                        </Typography>
                                        <Typography textAlign="left" paragraph>
                                            Αν σας άρεσε η πλατφόρμα και έχετε καλές γνώσεις HTML/CSS μπορείτε προαιρετικά να δοκιμάσετε να
                                            λύσετε και πιο δύσκολες ασκήσεις με την επιλογή "Επιπλέον ασκήσεις" από την αρχική σελίδα.
                                        </Typography>
                                        <Typography textAlign="left" paragraph>
                                            Σας ευχαριστώ για την συμμετοχή.
                                        </Typography>
                                    </Box>
                                </Container>
                            </main>
                        </>
                    }
                />
            </Routes>
        </>
    );
};

const ExercisePageWrapper: FunctionComponent<{
    redirect: FunctionComponentElement<RedirectProps> | null;
    nextExerciseId: number | boolean;
}> = (props) => {
    const id = useParams().id;
    const isFirstMount = useRef(true);
    useEffect(() => {
        isFirstMount.current = false;
    }, []);

    if(isFirstMount.current && props.nextExerciseId && props.nextExerciseId !== Number(id)) {
        const clonedRedirect = cloneElement(props.redirect!, {
            to: '../../' + props.redirect?.props.to,
        });

        return <>{clonedRedirect}</>;
    }

    return <>{props.children}</>;
};