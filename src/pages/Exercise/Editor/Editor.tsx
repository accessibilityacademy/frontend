import { css } from '@emotion/react';
import { Box, CircularProgress, NoSsr, useTheme } from '@material-ui/core';
import { FunctionComponent, lazy, memo, Suspense, useCallback, useEffect, useRef, useState } from 'react';
import { EditorHeader } from './EditorHeader';
import { MonacoEditorWrapperRef } from './MonacoEditor';

const MonacoEditorWrapper = lazy(() => import('./MonacoEditor').then(x => ({ default: x.MonacoEditorWrapper })));

export const EmptyEditorContent = `<style>

</style>

<script>

</script>`;

export type EditorModels = 'html' | 'styles' | 'script';

const useStyles = () => {
    const theme = useTheme();
    return {
        container: css({
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            minHeight: 250,
        }),
        editorPlaceholder: css({
            display: 'flex',
            backgroundColor: theme.palette.background.paper,
            minHeight: 250,
            flex: '1 1 1px',
            justifyContent: 'center',
            alignItems: 'center',
        }),
    };
};

interface EditorProps {
    onNext: () => void;
    onChange: (value: string) => void;
    onSubmit: (value: string) => Promise<void>;
    onReset: () => void;
    initialValue: string;
    value: string;
    submitted: boolean; // Whether the answer is submitted and was correct
    compact: boolean;
}

export const Editor: FunctionComponent<EditorProps> = memo((props) => {

    const [submitting, setSubmitting] = useState(false);
    const [selectedEditor, setSelectedEditor] = useState<EditorModels>('html');

    const styles = useStyles();

    const onSubmit = useCallback(async () => {
        setSubmitting(true);
        const onSubmit = props.onSubmit;
        await onSubmit(props.value);
        setSubmitting(false);
    }, [props.onSubmit, props.value]);

    const onChange = useCallback((value) => {
        editorCurrentText.current = value;
        const onChange = props.onChange;
        onChange(value);
    }, [props.onChange]);

    const onReset = useCallback(() => {
        editorWrapperRef.current?.resetEditor();
        const onReset = props.onReset;
        onReset();
    }, [props.onReset]);

    const editorWrapperRef = useRef<MonacoEditorWrapperRef | null>(null);

    const editorCurrentText = useRef('');
    useEffect(() => {
        if(editorCurrentText.current !== props.value) {
            editorWrapperRef.current?.restoreState(props.value);
        }
    }, [props.value]);

    useEffect(() => {
        setSelectedEditor('html');
    }, [props.initialValue]);

    return (
        <Box css={styles.container}>
            <EditorHeader
                submitted={props.submitted}
                submitting={submitting}
                onResetEditor={onReset}
                selectedEditor={selectedEditor}
                onSelectedEditor={(val) => setSelectedEditor(val)}
                onSubmit={onSubmit}
                onNext={props.onNext}
                compact={props.compact}
            />
            <NoSsr fallback={
                <Box css={styles.editorPlaceholder}>
                    <CircularProgress />
                </Box>
            }>
                <Suspense fallback={
                    <Box css={styles.editorPlaceholder}>
                        <CircularProgress />
                    </Box>
                }>
                    <MonacoEditorWrapper
                        ref={editorWrapperRef}
                        readOnly={props.submitted ? 'visible' : submitting || !props.initialValue ? 'darkened' : false }
                        selectedEditor={selectedEditor}
                        initialValue={props.initialValue}
                        compact={props.compact}
                        onChange={onChange}
                    />
                </Suspense>
            </NoSsr>
        </Box>
    );
});