import { Box, useTheme } from '@material-ui/core';
import { forwardRef, memo, useCallback, useEffect, useImperativeHandle, useRef } from 'react';
import MonacoEditor, { monaco } from 'react-monaco-editor';
import { EditorModels } from './Editor';

monaco.languages.typescript.javascriptDefaults.setDiagnosticsOptions({
    noSemanticValidation: true,
    noSyntaxValidation: false,
});

monaco.languages.typescript.javascriptDefaults.setCompilerOptions({
    target: monaco.languages.typescript.ScriptTarget.ES2015,
    allowNonTsExtensions: true,
    allowJs: true,
});


// https://github.com/microsoft/monaco-editor/issues/1873#issuecomment-629113547
/**
 * @param keyCode {monaco.KeyCode}
 */
function usuallyProducesCharacter(keyCode: number) {
    if (keyCode >= monaco.KeyCode.KEY_0 && keyCode <= monaco.KeyCode.KEY_9) {
        return true;
    }
    if (keyCode >= monaco.KeyCode.NUMPAD_0 && keyCode <= monaco.KeyCode.NUMPAD_9) {
        return true;
    }
    if (keyCode >= monaco.KeyCode.KEY_A && keyCode <= monaco.KeyCode.KEY_Z) {
        return true;
    }
    switch(keyCode) {
        // case monaco.KeyCode.Tab:
        case monaco.KeyCode.Enter:
        case monaco.KeyCode.Space:
        case monaco.KeyCode.Delete:
        case monaco.KeyCode.US_SEMICOLON:
        case monaco.KeyCode.US_EQUAL:
        case monaco.KeyCode.US_COMMA:
        case monaco.KeyCode.US_MINUS:
        case monaco.KeyCode.US_DOT:
        case monaco.KeyCode.US_SLASH:
        case monaco.KeyCode.US_BACKTICK:
        case monaco.KeyCode.US_OPEN_SQUARE_BRACKET:
        case monaco.KeyCode.US_BACKSLASH:
        case monaco.KeyCode.US_CLOSE_SQUARE_BRACKET:
        case monaco.KeyCode.US_QUOTE:
        case monaco.KeyCode.OEM_8:
        case monaco.KeyCode.OEM_102:
            return true;
    }
    return false;
}

interface MonacoEditorWrapperProps {
    onChange: (val: string) => void;
    initialValue: string;
    compact: boolean;
    selectedEditor: EditorModels;
    readOnly: 'darkened' | 'visible' | false;
}

export interface MonacoEditorWrapperRef {
    resetEditor(): void;
    restoreState(value: string): void;
}

export const MonacoEditorWrapper = memo(forwardRef<MonacoEditorWrapperRef, MonacoEditorWrapperProps>((props, ref) => {
    const themeMode = useTheme().palette.mode;

    useImperativeHandle(ref, () => ({
        resetEditor: () => {
            prepareEditor(props.initialValue, true);
        },
        restoreState: (value: string) => {
            prepareEditor(value ?? '', false);
        },
    }));

    const onChange = useCallback(() => {
        const onChange = props.onChange;
        onChange(
            '<style>\n' + editorModels.current.styles.getValue() + '\n</style>\n' +
            editorModels.current.html.getValue() +
            '\n<script>\n' + editorModels.current.script.getValue() + '\n</script>',
        );
    }, [props.onChange]);

    const editor = useRef<MonacoEditor>(null!);

    useEffect(() => {
        editor.current.editor?.setModel(editorModels.current[props.selectedEditor]);
    }, [props.selectedEditor]);

    useEffect(() => {
        const editorInstance = editor.current.editor;
        if(!editorInstance) return;

        const messageContribution = editorInstance.getContribution('editor.contrib.messageController');
        const disposable = editorInstance.onDidAttemptReadOnlyEdit(() => {
            (messageContribution as unknown as { showMessage: (message: string, pos: monaco.Position | null) => void}).showMessage(
                'The answer was correct. You cannot edit it further',
                editorInstance.getPosition(),
            );
        });

        const listener = editorInstance.onKeyDown((e) => {
            if(props.readOnly === 'visible') { // darkened is when it's submitting but can't show a message due to a bug
                if (!e.ctrlKey && !e.altKey && !e.metaKey) {
                    if (usuallyProducesCharacter(e.keyCode)) {
                        editorInstance.trigger('', 'type', { text: 'nothing' });
                    }
                }
            }
        });

        return () => {
            disposable.dispose();
            listener.dispose();
        };
    }, [props.readOnly]);


    const prepareEditor = useCallback((from: string, removeInitialIndent: boolean) => {
        const previousModel = editor.current.editor?.getModel();
        editor.current.editor?.setModel(null);

        const beginIndexStyles = from.indexOf('<style>\n');
        const endIndexStyles = from.indexOf('\n</style>\n', beginIndexStyles);

        const beginIndexScript = from.indexOf('\n<script>\n');
        const endIndexScript = from.indexOf('\n</script>', beginIndexScript);

        editorModels.current.html.setValue(
            from
                .replace(beginIndexStyles !== -1 ? from.substring(beginIndexStyles, endIndexStyles + 10) : '', '')
                .replace(beginIndexScript !== -1 ? from.substring(beginIndexScript, endIndexScript + 10) : '', ''),
        );

        editorModels.current.styles.setValue(
            beginIndexStyles !== -1 ? from.substring(
                beginIndexStyles + 8,
                endIndexStyles,
            ) : '',
        );

        const scriptContent = beginIndexScript !== -1 ? from.substring(
            beginIndexScript + 10,
            endIndexScript,
        ) : '';

        editorModels.current.script.setValue(
            removeInitialIndent ? scriptContent.trim().replaceAll('\n    ', '\n') : scriptContent,
        );

        editor.current.editor?.setModel(previousModel!);
        onChange();
    }, [onChange]);

    useEffect(() => {
        prepareEditor(props.initialValue, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.initialValue]);

    const editorModels = useRef<{ [key in EditorModels]: monaco.editor.ITextModel }>({
        html: monaco.editor.createModel('', 'html'),
        styles: monaco.editor.createModel('', 'css'),
        script: monaco.editor.createModel('', 'javascript'),
    });

    const editorContainerRef = useRef<HTMLDivElement>(null!);

    useEffect(() => {
        const observer = new ResizeObserver(e => {
            e.forEach(el => {
                if(el.target === editorContainerRef.current) {
                    if(!el.contentBoxSize) {
                        editor.current?.editor?.layout();
                    } else if(el.contentBoxSize[0]) {
                        editor.current?.editor?.layout({
                            width: el.contentBoxSize[0]?.inlineSize,
                            height: el.contentBoxSize[0]?.blockSize,
                        });
                    } else {
                        editor.current?.editor?.layout({
                            width: (el.contentBoxSize as unknown as ResizeObserverSize).inlineSize,
                            height: (el.contentBoxSize as unknown as ResizeObserverSize).blockSize,
                        });
                    }
                }
            });
        });
        observer.observe(editorContainerRef.current);
        return () => {
            observer.disconnect();
        };
    }, []);


    const isEditorReadOnly = !!props.readOnly;

    return (
        <Box
            ref={editorContainerRef}
            sx={{
                flex: '1 1 1px',
                overflow: 'hidden', // Sometimes a weird horizontal bar appears on readonly editor
                filter: props.readOnly === 'darkened' ? 'opacity(0.65)' : 'unset',
                '@media screen and (max-height: 299px)': {
                    maxHeight: '100vh',
                    minHeight: '100vh',
                },
            }}
            css={isEditorReadOnly ? {
                '& .monaco-editor .cursors-layer > .cursor': {
                    display: 'none !important',
                },
            } : null}
        >
            <MonacoEditor
                ref={editor}
                height="100%"
                theme={themeMode === 'dark' ? 'vs-dark' : 'vs'}
                css={{
                    '.monaco-editor.rename-box, .monaco-hover': {
                        top: 0,
                    },
                }}
                options={{
                    minimap: { enabled: false },
                    readOnly: isEditorReadOnly,
                    wrappingIndent: 'same',
                    wordWrap: 'on',
                    padding: { top: 8, bottom: -8 },
                    fontSize: parseFloat(getComputedStyle(document.documentElement).fontSize) - 2,
                    model: editorModels.current.html,
                    fixedOverflowWidgets: true,
                    lineDecorationsWidth: props.compact ? 0 : undefined,
                    lineNumbersMinChars: props.compact ? 3 : undefined,
                }}
                onChange={onChange}
            />
        </Box>
    );
}));