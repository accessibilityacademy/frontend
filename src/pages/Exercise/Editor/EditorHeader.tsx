import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Grid,
    IconButton,
    Tab,
    Tabs,
    TabsActions,
    Tooltip,
    Typography,
} from '@material-ui/core';
import { visuallyHidden } from '@material-ui/utils';
import { RestorePageOutlined } from '@material-ui/icons';
import { LoadingButton } from '@material-ui/lab';
import { FunctionComponent, memo, useEffect, useMemo, useRef, useState } from 'react';
import { ReactComponent as HTML } from '../../../components/icons/html.svg';
import { ReactComponent as CSS } from '../../../components/icons/css.svg';
import { ReactComponent as JavaScript } from '../../../components/icons/javascript.svg';
import { css } from '@emotion/react';
import { EditorModels } from './Editor';



const useStyles = (compact: boolean) => {
    if(compact) {
        return {
            tab: css({
                minWidth: '80px !important',
                minHeight: 0,
                lineHeight: 'normal',
            }),
            tabIcon: css({
                width: 24,
                margin: '0 !important',
            }),
            sameSize: css({
                '& .MuiTab-root': {
                    flex: '1 1 0',
                },
            }),
        };
    } else {
        return {
            tab: css({ minWidth: '80px !important' }),
            tabIcon: css({
                width: 36,
            }),
            sameSize: css({
                '& .MuiTab-root': {
                    flex: '1 1 0',
                },
            }),
        };
    }
};

interface EditorHeaderProps {
    onNext: () => void;
    onSubmit: () => void;
    onResetEditor: () => void;
    selectedEditor: EditorModels;
    onSelectedEditor: (editorModel: EditorModels) => void;
    submitting: boolean;
    submitted: boolean; // Whether the answer is submitted and was correct
    compact: boolean;
}

export const EditorHeader: FunctionComponent<EditorHeaderProps> = memo((props) => {

    const styles = useStyles(props.compact);

    const editorSelectorContainerRef = useRef<HTMLDivElement>(null!);
    const editorSelectorRef = useRef<TabsActions>(null!);

    useEffect(() => {
        const observer = new ResizeObserver(e => {
            e.forEach(el => {
                if(el.target === editorSelectorContainerRef.current) {
                    editorSelectorRef.current?.updateIndicator();
                    editorSelectorRef.current?.updateScrollButtons();
                }
            });
        });
        observer.observe(editorSelectorContainerRef.current);
        return () => {
            observer.disconnect();
        };
    }, []);



    const [isResetEditorDialogOpen, setIsResetEditorDialogOpen] = useState(false);


    const ResetEditorDialog = useMemo(() => {

        const onResetEditorDialogClose = (confirm: boolean) => {
            setIsResetEditorDialogOpen(false);
            if (confirm) {
                const onResetEditor = props.onResetEditor;
                onResetEditor();
            }
        };

        return (
            <Dialog
                onClose={() => setIsResetEditorDialogOpen(false)}
                open={isResetEditorDialogOpen}
                aria-labelledby="reset-editor-alert-title"
                aria-describedby="reset-editor-alert-description"
            >
                <DialogTitle id="reset-editor-alert-title">Reset editor?</DialogTitle>
                <DialogContent>
                    <DialogContentText id="reset-editor-alert-description">
                        Reset editor and start the exercise from the beginning? You will lose any progress you made in solving the exercise.
                    </DialogContentText>
                    <DialogActions>
                        <Button onClick={() => onResetEditorDialogClose(false)} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={() => onResetEditorDialogClose(true)} color="secondary">
                            Reset
                        </Button>
                    </DialogActions>
                </DialogContent>
            </Dialog>
        );
    }, [isResetEditorDialogOpen, props.onResetEditor]);

    return (
        <>
            <Typography component="h1" sx={{ ...visuallyHidden }}>Editor</Typography>
            <Grid
                container
                justifyContent="space-between"
                alignItems="center"
                alignContent="end"
                wrap="nowrap"
                sx={{ overflowX: 'auto', overflowY: 'hidden' }}
            >
                <Grid ref={editorSelectorContainerRef} minWidth={props.compact ? '80px' : '160px'} item>
                    <Tabs
                        action={editorSelectorRef}
                        value={props.selectedEditor}
                        onChange={(e, val) => {
                            props.onSelectedEditor(val);
                        }}
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="editor selector"
                        variant="scrollable"
                        css={styles.sameSize}
                    >
                        <Tab
                            css={styles.tab}
                            icon={<HTML css={styles.tabIcon} />} label="HTML" value="html"
                        />
                        <Tab
                            css={styles.tab}
                            icon={<CSS css={styles.tabIcon} />} label="CSS" value="styles"
                        />
                        <Tab
                            css={styles.tab}
                            icon={<JavaScript css={styles.tabIcon} />} label="JavaScript" value="script"
                        />
                    </Tabs>
                </Grid>
                <Grid item flexGrow={1} css={{ textAlign: 'right' }}>
                    { ResetEditorDialog }
                    <Box sx={{ p: 1, flexGrow: 0, marginLeft: 'auto', display: 'inline-flex' }}>
                        <Tooltip title="Reset the editor and restart the exercise">
                            <IconButton
                                css={(theme) => ({ marginRight: theme.spacing(1) })}
                                aria-label="Reset the editor and restart the exercise"
                                onClick={() => setIsResetEditorDialogOpen(true)}
                                disabled={props.submitted}
                                size={ props.compact ? 'small' : 'medium' }
                            >
                                <RestorePageOutlined />
                            </IconButton>
                        </Tooltip>
                        {!props.submitted ?
                            <LoadingButton
                                loading={props.submitting}
                                variant="contained"
                                onClick={props.onSubmit}
                                size={ props.compact ? 'small' : 'medium' }
                            >
                                Submit
                            </LoadingButton> :
                            <Button
                                variant="contained"
                                onClick={props.onNext}
                                size={ props.compact ? 'small' : 'medium' }
                                sx={{
                                    backgroundColor: 'success.main',
                                    color: 'success.contrastText',
                                    '&:hover': {
                                        backgroundColor: 'success.dark',
                                    },
                                }}
                            >
                                Next
                            </Button>
                        }
                    </Box>
                </Grid>
            </Grid>
        </>
    );
});