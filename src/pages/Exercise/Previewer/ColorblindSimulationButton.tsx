import { MouseEvent, memo, useState, forwardRef } from 'react';
import { ArrowDropDown } from '@material-ui/icons';
import { Menu, MenuItem, ToggleButton } from '@material-ui/core';
import { ReactComponent as VisualImpairment } from '../../../components/icons/visual-impairment.svg';
import { Deficiency } from '../../../components/IFrame/IFrame';

interface ColorblindSimulationButtonProps {
    onFilterSelected: (val: Deficiency) => void;
    filter: Deficiency;
    className?: string;
}

export const ColorblindSimulationButton =  memo(forwardRef<HTMLButtonElement, ColorblindSimulationButtonProps>((props, ref) => {

    const { filter, onFilterSelected, ...rest } = props;

    const [open, setOpen] = useState(false);
    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

    const openColorblindMenu = (e: MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(e.currentTarget);
        setOpen(true);
    };
    const handleClose = (value: Deficiency | null) => {
        setOpen(false);
        if(value) {
            onFilterSelected(value);
        }
    };

    const ColorblindDropdownMenu = (
        <Menu
            anchorEl={anchorEl}
            open={open}
            onClose={() => handleClose(null)}
            css={theme => ({ zIndex: theme.zIndex.tooltip + 1 })}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
        >
            <MenuItem onClick={() => handleClose('none')} value="none" selected={filter === 'none'}>None</MenuItem>
            <MenuItem onClick={() => handleClose('protanopia')} value="protanopia" selected={filter === 'protanopia'}>
                Protanopia
            </MenuItem>
            <MenuItem onClick={() => handleClose('deuteranopia')} value="deuteranopia" selected={filter === 'deuteranopia'}>
                Deuteranopia
            </MenuItem>
            <MenuItem onClick={() => handleClose('tritanopia')} value="tritanopia" selected={filter === 'tritanopia'}>
                Tritanopia
            </MenuItem>
            <MenuItem onClick={() => handleClose('achromatopsia')} value="achromatopsia" selected={filter === 'achromatopsia'}>
                Achromatopsia
            </MenuItem>
        </Menu>
    );

    return (
        <>
            <ToggleButton
                {...rest}
                ref={ref}
                onClick={openColorblindMenu}
                value="colorblind-filter"
                selected={open || filter !== 'none'}
            >
                <VisualImpairment width={24} height={24} fill="currentColor" />
                <ArrowDropDown sx={{ ml: 0.5 }} />
            </ToggleButton>
            { ColorblindDropdownMenu }
        </>
    );
}));