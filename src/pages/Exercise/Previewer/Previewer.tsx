import { css } from '@emotion/react';
import { Box, Card, CardContent, Divider, Paper, ToggleButton, ToggleButtonGroup, Tooltip, Typography, useTheme } from '@material-ui/core';
import { FunctionComponent, memo, useEffect, useMemo, useRef, useState } from 'react';
import { Deficiency, IFrame, IFrameRef } from '../../../components/IFrame/IFrame';
import { FormatSize, Replay, StayPrimaryLandscape } from '@material-ui/icons';
import { ColorblindSimulationButton } from './ColorblindSimulationButton';
import { useStore } from '../../../services/ServiceProvider';
import { StoreKey } from '../../../services/StoreService';

interface PreviewerProps {
    html: string;
}

const useStyles = () => {
    const theme = useTheme();
    return useMemo(() => {
        return {
            container: css({
                height: '100%',
                minHeight: 250,
                // backgroundColor: theme.palette.background.default,
                // backgroundColor: '#232323',
            }),
            radioLabel: css({
                '& .MuiFormControlLabel-label': {
                    marginTop: 2,
                },
                marginLeft: -6,
            }),
            radioInput: css({
                padding: 4,
            }),
            fullHeight: css({
                height: '100%',
            }),
            cardContent: css({
                padding: theme.spacing(2),
                '&:last-child': {
                    paddingBottom: theme.spacing(2),
                },
            }),
            previewerContainer: css({
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
                border: `1px solid ${theme.palette.divider}`,
            }),
            zIndexFix: css({
                zIndex: 1,
            }),
            toolbar: css({
                backgroundColor: theme.palette.background.default,
                display: 'flex',
                flexWrap: 'wrap',
                borderBottom: `1px solid ${theme.palette.divider}`,
            }),
            heading: css({
                marginLeft: theme.spacing(1),
                marginRight: 'auto',
                marginTop: 11,
                marginBottom: 11,
                alignSelf: 'center',
            }),
            divider: css({
                margin: theme.spacing(1, 0.5),
            }),
            iFrameContainer: css({
                minHeight: 0, // min height controlled by previewer container, not the iframe
                flexGrow: 1,
                flexBasis: 1,
                display: 'flex',
                flexDirection: 'column',
            }),
            buttonGroup: css({
                '& .MuiToggleButtonGroup-grouped': {
                    margin: theme.spacing(0.5),
                    border: 0,
                },
            }),
            landscapeMobileContainer: css({
                overflow: 'auto',
            }),
            landscapeMobile: css({
                boxSizing: 'content-box',
                height: 0.5625 * 600 + 2,
                width: 600 + 2,
            }),
        };
    }, [theme]);
};

export const Previewer: FunctionComponent<PreviewerProps> = memo((props) => {

    const store = useStore();
    const styles = useStyles();
    const theme = useTheme();

    const [colorFilter, setColorFilter] = useState<Deficiency>('none');
    const [largeFontSize, setLargeFontSize] = useState(false);
    const [mobileLandscapeKeyboard, setMobileLandscapeKeyboard] = useState(false);
    useEffect(() => {
        setColorFilter((store.getValue(StoreKey.COLOR_FILTER, 'string') as Deficiency) ?? 'none');
        setLargeFontSize(store.getValue(StoreKey.BIG_FONT_SIZE, 'boolean') ?? false);
        setMobileLandscapeKeyboard(store.getValue(StoreKey.MOBILE_LANDSCAPE_KEYBOARD, 'boolean') ?? false);
    }, [store]);

    const iFrameRef = useRef<IFrameRef>(null!);

    const html = largeFontSize ?
        '<style>html { font-size : 200%; }</style>\n' + props.html :
        props.html;


    const header = useMemo(() => (
        <Paper elevation={0} css={styles.toolbar} square>
            <Typography variant="h6" component="h1" css={styles.heading}>Preview Page</Typography>
            <Box display="flex" flexWrap="wrap">
                <ToggleButtonGroup css={styles.buttonGroup}>
                    <Tooltip title="Refresh">
                        <ToggleButton aria-pressed={undefined} value="refresh" onClick={() => iFrameRef.current.reload()}>
                            <Replay />
                        </ToggleButton>
                    </Tooltip>
                </ToggleButtonGroup>

                <Divider flexItem css={styles.divider} orientation="vertical" />

                <ToggleButtonGroup css={styles.buttonGroup}>
                    <Tooltip title="Colorblind Simulation">
                        <ColorblindSimulationButton
                            filter={colorFilter}
                            onFilterSelected={(val) => {
                                store.storeValue(StoreKey.COLOR_FILTER, val);
                                setColorFilter(val);
                            }}
                        />
                    </Tooltip>
                    <Tooltip title="Increase font size">
                        <ToggleButton
                            value="font-size"
                            onClick={() => {
                                setLargeFontSize(x => {
                                    store.storeValue(StoreKey.BIG_FONT_SIZE, !x);
                                    return !x;
                                });
                            }}
                            selected={largeFontSize}
                        >
                            <FormatSize />
                        </ToggleButton>
                    </Tooltip>
                    <Tooltip title="Simulate landscape oriented phone with open keyboard">
                        <ToggleButton
                            value="landscape-phone-keyboard"
                            onClick={() => {
                                setMobileLandscapeKeyboard(x => {
                                    store.storeValue(StoreKey.MOBILE_LANDSCAPE_KEYBOARD, !x);
                                    return !x;
                                });
                            }}
                            selected={mobileLandscapeKeyboard}
                        >
                            <StayPrimaryLandscape />
                        </ToggleButton>
                    </Tooltip>
                </ToggleButtonGroup>
            </Box>
        </Paper>
    ), [colorFilter, largeFontSize, mobileLandscapeKeyboard, store, styles.buttonGroup, styles.divider, styles.heading, styles.toolbar]);

    return (
        <Card
            elevation={theme.palette.mode === 'dark' ? 6 : 0}
            css={[styles.container, mobileLandscapeKeyboard ? styles.landscapeMobileContainer : null]}
            square
        >
            <CardContent css={[styles.fullHeight, styles.cardContent, mobileLandscapeKeyboard ? styles.landscapeMobile : null]}>
                <Box css={styles.previewerContainer}>
                    { header }
                    <Box css={styles.iFrameContainer}>
                        <IFrame ref={iFrameRef} html={html} colorblindnessSimulation={colorFilter} />
                        {mobileLandscapeKeyboard ?
                            <img src={`/${theme.palette.mode}-mobile-keyboard.png`} alt="" /> :
                            null
                        }
                    </Box>
                </Box>
            </CardContent>
        </Card>
    );
});