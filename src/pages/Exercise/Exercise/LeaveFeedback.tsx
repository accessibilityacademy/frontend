import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogProps,
    DialogTitle,
    FormControlLabel,
    IconButton,
    Radio,
    RadioGroup,
    TextField,
    Typography,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { LoadingButton } from '@material-ui/lab';
import React, { useState } from 'react';
import { Feedback } from '../../../services/ExerciseService';

interface LeaveFeedbackProps {
    open: boolean;
    onClose: () => void;
    container?: DialogProps['container'];
    onSubmit: (feedback: Feedback) => Promise<void>;
}

export const LeaveFeedback = (props: LeaveFeedbackProps) => {
    const [selectedReportOption, setSelectedReportOption] = useState<Feedback['type'] | ''>('');
    const [feedbackDetails, setFeedbackDetails] = useState('');

    const [canShowError, setCanShowError] = useState(false);

    const [loading, setLoading] = useState(false);

    const onSubmit = async () => {
        if(!selectedReportOption) return;
        setLoading(true);
        await props.onSubmit({
            type: selectedReportOption,
            details: feedbackDetails,
        });
        setLoading(false);
        props.onClose();
    };

    return (
        <Dialog
            container={props.container}
            onClose={props.onClose}
            open={props.open}
            aria-labelledby="leave-feedback-title"
        >
            <DialogTitle id="leave-feedback-title" disableTypography>
                <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Typography component="h1" variant="h6">
                        Leave Feedback
                    </Typography>
                    <IconButton edge="end" onClick={props.onClose}>
                        <Close />
                    </IconButton>
                </Box>
            </DialogTitle>
            <DialogContent>
                <RadioGroup
                    aria-required="true"
                    aria-label="feedback type"
                    value={selectedReportOption} onChange={(e) => setSelectedReportOption(e.target.value as Feedback['type'])}
                >
                    <FormControlLabel
                        value="false-negative-solution"
                        control={<Radio />}
                        label="Solution not accepted"
                    />
                    <FormControlLabel
                        value="false-positive-solution"
                        control={<Radio />}
                        label="Wrong solution accepted"
                    />
                    <FormControlLabel value="other" control={<Radio />} label="Other" />
                </RadioGroup>
                <TextField
                    css={{ marginTop: 8 }}
                    value={feedbackDetails}
                    onChange={(e) => setFeedbackDetails(e.target.value)}
                    id="leave-feedback-details"
                    label="Provide details"
                    multiline
                    fullWidth
                    error={selectedReportOption === 'other' && !feedbackDetails && canShowError}
                    onBlur={() => setCanShowError(true)}
                />
                <br />
                <Typography variant="caption">
                    The contents of the code editor will also be submitted.
                    Make sure that you have typed the problematic code before submitting the feedback.
                </Typography>
                <DialogActions>
                    <Button onClick={props.onClose} color="secondary">
                        Cancel
                    </Button>
                    <LoadingButton
                        loading={loading}
                        disabled={!selectedReportOption || (selectedReportOption === 'other' && !feedbackDetails)}
                        onClick={onSubmit}
                        color="primary"
                    >
                        Submit
                    </LoadingButton>
                </DialogActions>
            </DialogContent>
        </Dialog>
    );
};
