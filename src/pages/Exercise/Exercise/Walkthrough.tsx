import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    MobileStepper,
    ModalProps,
    Step,
    StepButton,
    Stepper,
    Typography,
    useTheme,
} from '@material-ui/core';
import { Close, KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import { Markdown } from '../../../components/Markdown';
import { WalkthroughStep } from '../../../services/ExerciseService';

interface WalkthroughProps {
    activeStep: number;
    onChange: (activeStep: number) => void;
    walkthroughSteps: WalkthroughStep[];
    open: boolean;
    mobile?: boolean;
    onClose: () => void;
    container?: ModalProps['container'];
}

export const Walkthrough = (props: WalkthroughProps) => {

    const theme = useTheme();

    let stepper: JSX.Element;
    let actions: JSX.Element | null;

    if(props.mobile) {
        stepper = (
            <>
                { props.walkthroughSteps[0]?.title ?
                    <Typography component="h2" variant="body1" paragraph>
                        Step {props.activeStep + 1}: {props.walkthroughSteps[props.activeStep]?.title}
                    </Typography> :
                    null
                }
                <Markdown>
                    {props.walkthroughSteps[props.activeStep]?.step ?? 'No walkthrough found!'}
                </Markdown>
            </>
        );

        actions = (
            <DialogActions css={{ display: 'block', padding: 0 }}>
                <MobileStepper
                    steps={props.walkthroughSteps.length}
                    position="static"
                    activeStep={props.activeStep}
                    backButton={
                        <Button
                            onClick={() => props.onChange(props.activeStep - 1)}
                            disabled={props.activeStep === 0}
                        >
                            <KeyboardArrowLeft />
                            Back
                        </Button>
                    }
                    nextButton={
                        <Button
                            onClick={() => props.onChange(props.activeStep + 1)}
                            disabled={props.activeStep === props.walkthroughSteps.length - 1}
                        >
                            Next
                            <KeyboardArrowRight />
                        </Button>
                    }
                />
            </DialogActions>
        );
    } else {
        stepper = (
            <>
                <Stepper css={{ paddingBottom: 24, paddingTop: 24, marginTop: -16 }} nonLinear activeStep={props.activeStep}>
                    {props.walkthroughSteps.map((step, i) => (
                        <Step key={step.id}>
                            <StepButton onClick={() => props.onChange(i)}>{step.title ?? 'Step ' + (i + 1)}</StepButton>
                        </Step>
                    ))}
                </Stepper>
                <Markdown>
                    {props.walkthroughSteps[props.activeStep]?.step ?? 'No walkthrough found!'}
                </Markdown>
            </>
        );

        actions = (
            <DialogActions css={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button
                    variant="outlined"
                    onClick={() => props.onChange(props.activeStep - 1)}
                    disabled={props.activeStep === 0}
                    startIcon={<KeyboardArrowLeft />}
                >
                    Back
                </Button>
                <Button
                    variant="outlined"
                    onClick={() => props.onChange(props.activeStep + 1)}
                    disabled={props.activeStep === props.walkthroughSteps.length - 1}
                    endIcon={<KeyboardArrowRight />}
                >
                    Next
                </Button>
            </DialogActions>
        );
    }

    return (
        <Dialog
            PaperProps={{ sx: { backgroundColor: theme.palette.mode === 'light' ? 'background.default' : undefined } }}
            fullScreen={props.mobile}
            maxWidth="md"
            fullWidth
            onClose={props.onClose}
            open={props.open}
            container={props.container}
            aria-labelledby="walkthrough-dialog-title"
        >
            <DialogTitle
                css={(theme) => ({ padding: theme.spacing(1, props.mobile ? 2 : 3) })}
                disableTypography
                id="walkthrough-dialog-title"
            >
                <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Typography component="h1" variant="h6">
                        Walkthrough
                    </Typography>
                    <IconButton edge="end" onClick={props.onClose}>
                        <Close />
                    </IconButton>
                </Box>
            </DialogTitle>
            <DialogContent dividers css={props.mobile ? (theme) => ({ padding: theme.spacing(2, 2) }) : null}>
                { props.walkthroughSteps.length > 1 ?
                    <>
                        { stepper }
                    </> :
                    <>
                        { props.walkthroughSteps[0]?.title ?
                            <Typography component="h2" variant="body1" paragraph>
                                {props.walkthroughSteps[0]?.title}
                            </Typography> :
                            null
                        }
                        <Markdown>
                            {props.walkthroughSteps[0]?.step ?? 'No walkthrough found!'}
                        </Markdown>
                    </>
                }
            </DialogContent>
            { props.walkthroughSteps.length > 1 && actions }
        </Dialog>
    );
};
