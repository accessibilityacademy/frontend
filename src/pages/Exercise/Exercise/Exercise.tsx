import {
    Alert,
    Box,
    Button,
    CssBaseline,
    darken,
    Divider,
    IconButton,
    lighten,
    Skeleton,
    ThemeProvider,
    Tooltip,
    Typography,
    useMediaQuery,
    useTheme,
} from '@material-ui/core';
import { visuallyHidden } from '@material-ui/utils';
import React, { FunctionComponent, lazy, memo, Suspense, useCallback, useEffect, useRef, useState } from 'react';
import { Markdown } from '../../../components/Markdown';
import { AsyncHttpTask, HttpTaskState, isErrored, isLoaded, isLoading, StateType } from '../../../helpers/AsyncHttpTask';
import { AnswerResult, ExerciseDetails, Feedback, WalkthroughStep } from '../../../services/ExerciseService';
import { CacheProvider, css } from '@emotion/react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useThemeService } from '../../../services/ThemeProvider';
import { useExerciseService } from '../../../services/ServiceProvider';
import { Walkthrough } from './Walkthrough';
import { OpenInNew } from '@material-ui/icons';
import { useAutoCleanSubscriptions } from '../../../helpers/useAutoCleanSubscriptions';
import { LoadingButton } from '@material-ui/lab';
import weakMemoize from '@emotion/weak-memoize';
import createCache from '@emotion/cache';
import { Popout } from 'react-portal-popout';
import { LeaveFeedback } from './LeaveFeedback';

const PopoutComponent = lazy(() => import('react-portal-popout').then(x => ({ default: x.Popout })));

const useStyles = () => {
    const theme = useTheme();

    return {
        popoutContainer: css({
            height: '100vh',
        }),
        scrollableContainer: css({
            overflowY: 'auto',
            height: '100%',
            position: 'relative', // used for visuallyHidden placement
        }),
        container: css({
            [theme.breakpoints.down('lg')]: {
                maxWidth: theme.breakpoints.values.md,
                margin: 'auto',
            },
            [theme.breakpoints.up('lg')]: {
                display: 'flex',
            },
            flexWrap: 'wrap',
            height: '100%',
        }),
        bodyContainer: css({
            padding: theme.spacing(2, 4),
        }),
        alert: css({
            '& .MuiAlert-message': {
                flexGrow: 1,
                minWidth: 0,
            },
            '&:not(:last-child)': {
                marginBottom: theme.spacing(1),
            },
            borderRadius: 4,
        }),
        column: css({
            flexBasis: '17.5%',
            flexGrow: 1,
        }),
        columnDivider: {
            [theme.breakpoints.up('lg')]: {
                borderRight: `1px solid ${theme.palette.divider}`,
            },
        },
        poppedOutMessageContainer: css({
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            height: '100%',
            overflowY: 'auto',
        }),
        red: css({
            color: theme.palette.mode === 'dark' ? lighten(theme.palette.error.main, 0.6) : darken(theme.palette.error.main, 0.3),
        }),
    };
};


const memoizedCreateCacheWithContainer = weakMemoize((container: HTMLElement) => createCache({ container, key: 'popout' }));

const PopoutEmotionProvider: FunctionComponent<{ containerHead: HTMLHeadElement }> = (props) => {
    return (
        <CacheProvider value={memoizedCreateCacheWithContainer(props.containerHead)}>
            {props.children}
        </CacheProvider>
    );
};

interface ExerciseProps {
    exercise: HttpTaskState<ExerciseDetails>;
    results: AnswerResult | null;
    newLayout: boolean;
    mobile?: boolean;
    onFeedbackSubmit: (feedback: Feedback) => Promise<void>;
}

export const Exercise: FunctionComponent<ExerciseProps> = memo((props) => {

    const styles = useStyles();

    const subs = useAutoCleanSubscriptions();

    const themeService = useThemeService();
    const exerciseService = useExerciseService();
    const navigate = useNavigate();
    const location = useLocation();

    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('md'));

    // eslint-disable-next-line @typescript-eslint/no-extra-parens
    const alertsRefs = useRef<(HTMLDivElement | null)[]>([]);

    const [, updateState] = useState({});
    const forceUpdate = useCallback(() => {
        setTimeout(() => {
            updateState({});
        }, 0);
        return null;
    }, []);

    const [poppedOut, setPoppedOut] = useState(false);
    const popRef = useRef<Popout>(null!);

    const [walkthroughSteps, setWalkthroughSteps] = useState<HttpTaskState<WalkthroughStep[]> | null>(null);
    const [displayWalkthrough, setDisplayWalkthrough] = useState(false);

    const openWalkthrough = useCallback(() => {
        if(isLoaded(props.exercise) && isMobile === (props.mobile ?? false)) {
            setWalkthroughSteps({ type: StateType.LOADING });
            subs.push(AsyncHttpTask(exerciseService.getExerciseWalkthrough(props.exercise.data.id)).subscribe(result => {
                setWalkthroughSteps(result);
                if(isLoaded(result)) {
                    setDisplayWalkthrough(true);
                }
            }));
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [exerciseService, props.exercise, props.mobile, isMobile]);

    useEffect(() => {
        if(isMobile !== (props.mobile ?? false)) {
            setDisplayWalkthrough(false);
        }
    }, [props.mobile, isMobile]);

    const onCloseWalkthrough = () => {
        if(!poppedOut && isLoaded(props.exercise)) {
            navigate(props.exercise.data.slug);
        } else {
            setDisplayWalkthrough(false);
        }
    };

    useEffect(() => {
        if(location.pathname.indexOf('walkthrough') !== -1) {
            openWalkthrough();
        } else {
            setDisplayWalkthrough(false);
        }
    }, [location, openWalkthrough]);


    const isFirstMount = useRef(true);
    useEffect(() => {
        if(!isFirstMount.current) {
            subs.cancel();
        } else {
            isFirstMount.current = false;
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [poppedOut]);

    useEffect(() => {
        if(isLoaded(props.exercise)) {
            alertsRefs.current = alertsRefs.current.slice(0, props.exercise.data.requirements.length);
        }
        const indexOfFirstWrongAnswer = props.results?.requirementResults.findIndex(x => !x.correct);
        if(typeof indexOfFirstWrongAnswer === 'number' && indexOfFirstWrongAnswer > -1) {
            setTimeout(() => {
                alertsRefs.current[indexOfFirstWrongAnswer]?.scrollIntoView({
                    behavior: 'smooth',
                    block: 'center',
                    inline: 'start',
                });
            }, 0);
        }
    }, [props.results, props.exercise]);

    useEffect(() => {
        if(isLoaded(props.exercise) && poppedOut) {
            const window = popRef.current?.child?.window;
            const document = popRef.current?.child?.document;

            if(window && document) {
                document.title = props.exercise.data.title;
                window.name = props.exercise.data.title;
                window.history.replaceState(
                    null,
                    props.exercise.data.title,
                    `/exercise-popup/${props.exercise.data.id}/${props.exercise.data.slug}`,
                );
            }
        }
    }, [props.exercise, poppedOut]);

    const scrollableContainerRef = useRef<HTMLElement | null>(null);
    const [walkthroughStep, setWalkthroughStep] = useState(0);
    useEffect(() => {
        setWalkthroughStep(0);
        scrollableContainerRef.current?.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    }, [props.exercise]);

    const [feedbackOpen, setFeedbackOpen] = useState(false);

    if (isErrored(props.exercise)) {
        return null;
    }

    const feedback = feedbackOpen && (
        <LeaveFeedback
            open={feedbackOpen}
            onSubmit={props.onFeedbackSubmit}
            container={poppedOut ? () => popRef.current?.child?.document.body ?? null : undefined}
            onClose={() => setFeedbackOpen(false)}
        />
    );

    const walkthrough = displayWalkthrough && (
        <Walkthrough
            activeStep={walkthroughStep}
            onChange={(activeStep) => setWalkthroughStep(activeStep)}
            mobile={props.mobile}
            open={displayWalkthrough}
            walkthroughSteps={isLoaded(walkthroughSteps) ? walkthroughSteps.data ?? [] : []}
            onClose={() => onCloseWalkthrough()}
            container={poppedOut ? () => popRef.current?.child?.document.body ?? null : undefined}
        />
    );


    const exercise = (
        <Box className="scrollable-container" ref={scrollableContainerRef} css={styles.scrollableContainer}>
            {feedback}
            {walkthrough}
            <Box css={props.newLayout && !poppedOut ? styles.container : undefined}>
                <Box css={[styles.column, styles.bodyContainer, props.newLayout ? styles.columnDivider : undefined]}>
                    {isLoading(props.exercise) ?
                        <>
                            <Typography variant="h5" component="h1" gutterBottom><Skeleton /></Typography>
                            <Typography variant="body1" paragraph><Skeleton/><Skeleton/><Skeleton/><Skeleton/><Skeleton/></Typography>
                            <Skeleton variant="rectangular" height={150} />
                        </> :
                        <>
                            <Box css={{ display: 'flex', justifyContent: 'space-between' }}>
                                <Typography variant="h5" component="h1" gutterBottom>{props.exercise.data.title}</Typography>
                                {!poppedOut && !props.mobile ?
                                    <Tooltip title="Popout exercise">
                                        <IconButton
                                            onClick={() => setPoppedOut(true)}
                                            aria-label="Popout exercise"
                                            edge="end"
                                            css={{ marginTop: '-8px', alignSelf: 'flex-start' }}
                                        >
                                            <OpenInNew />
                                        </IconButton>
                                    </Tooltip> :
                                    null
                                }
                            </Box>
                            <Markdown>
                                {props.exercise.data.body}
                            </Markdown>
                        </>
                    }
                </Box>
                <Divider />
                <Box css={[styles.column, styles.bodyContainer, props.newLayout ? styles.columnDivider : undefined]}>
                    <Typography variant="h5" component="h2" gutterBottom>
                        Requirements
                    </Typography>

                    {isLoading(props.exercise) ?
                        <>
                            <Skeleton css={styles.alert} height={70} variant="rectangular" />
                            <Skeleton css={styles.alert} height={70} variant="rectangular" />
                        </> :
                        props.exercise.data.requirements.map((requirement, i) => {
                            const isCorrect = props.results?.requirementResults.find(r => r.requirementId === requirement.id)?.correct;
                            let alertSeverity: 'success' | 'info' | 'error';
                            if(isCorrect) {
                                alertSeverity = 'success';
                            } else if (isCorrect === false) {
                                alertSeverity = 'error';
                            } else {
                                alertSeverity = 'info';
                            }
                            return (
                                <Alert
                                    key={requirement.id}
                                    variant="outlined"
                                    severity={alertSeverity}
                                    role={alertSeverity === 'error' ? 'alert' : 'region'}
                                    css={[
                                        styles.alert,
                                    ]}
                                    ref={el => alertsRefs.current[i] = el}
                                    // onClick={() => setExpanded(x => !x)}
                                > { /* @TODO: explain severity through ARIA? Switch to role=alert? */ }
                                    <Typography sx={{ ...visuallyHidden }}>
                                        {alertSeverity === 'info' ?
                                            'Pending requirement:' :
                                            alertSeverity === 'success' ?
                                                'Completed requirement: ' :
                                                'Incomplete requirement: '
                                        }
                                    </Typography>
                                    {/* <Collapse in={expanded}> */}
                                    <Markdown>
                                        {requirement.body}
                                    </Markdown>
                                    {/* </Collapse> */}
                                    {/* <AlertTitle aria-label="title of requirement">1. Alt attribute</AlertTitle>
                                    <Typography variant="body2" aria-label="description of requirement">
                                        Image must have a meaningful <strong>alt</strong>(ernative) attribute
                                    </Typography> */}
                                </Alert>
                            );
                        })
                    }
                </Box>
                <Divider />
                <Box css={styles.column}>
                    <Box css={styles.bodyContainer}>
                        <Typography variant="h5" component="h2" gutterBottom>
                            Need help?
                        </Typography>
                        <LoadingButton
                            variant="outlined"
                            fullWidth
                            loading={isLoading(walkthroughSteps)}
                            {...(poppedOut ? {
                                onClick: () => openWalkthrough(),
                            } : {
                                component: Link,
                                to: isLoading(props.exercise) ? '#' : `./${props.exercise?.data.slug}/walkthrough`,
                            })}
                        >
                            Show me the solution
                        </LoadingButton>
                        <Box css={styles.red} mt={1}>
                            <Button
                                variant="outlined"
                                color="inherit"
                                fullWidth
                                sx={{ borderColor: theme.palette.mode === 'light' ? 'red' : undefined }}
                                onClick={() => setFeedbackOpen(true)}
                            >
                                Leave feedback for this exercise
                            </Button>
                        </Box>
                    </Box>
                    <Divider />
                    <Box css={styles.bodyContainer}>
                        <Typography variant="h5" component="h2" gutterBottom>
                            Learning Material
                        </Typography>
                        {isLoading(props.exercise) ?
                            <ul>
                                <li><Typography variant="body2"><Skeleton/></Typography></li>
                                <li><Typography variant="body2"><Skeleton/></Typography></li>
                                <li><Typography variant="body2"><Skeleton/></Typography></li>
                            </ul> :
                            <>
                                <Markdown>
                                    {props.exercise.data.learningMaterial}
                                </Markdown>
                            </>
                        }
                    </Box>
                </Box>
            </Box>
        </Box>
    );

    return (
        <>
            { poppedOut ?
                <>
                    <Box css={styles.poppedOutMessageContainer}>
                        <Typography gutterBottom>Exercise popped out</Typography>
                        <Button variant="outlined" onClick={() => setPoppedOut(false)}>Restore</Button>
                    </Box>
                    <Suspense fallback={<></>}>
                        <PopoutComponent
                            ref={popRef}
                            title={
                                isLoading(props.exercise) ? 'Loading' : isErrored(props.exercise) ? 'Errored' : props.exercise.data.title
                            }
                            name={isLoading(props.exercise) ? 'Loading' : isErrored(props.exercise) ? 'Errored' : props.exercise.data.title}
                            onClose={() => {
                                setPoppedOut(false);
                                setDisplayWalkthrough(false);
                            }}
                            onBlocked={() => {
                                setPoppedOut(false);
                            }}
                        >
                            {!popRef.current ? forceUpdate() :
                                <PopoutEmotionProvider containerHead={popRef.current!.child!.document.head}>
                                    <ThemeProvider theme={themeService.theme}>
                                        <CssBaseline />
                                        <Box css={styles.popoutContainer}>
                                            {exercise}
                                        </Box>
                                    </ThemeProvider>
                                </PopoutEmotionProvider>
                            }
                        </PopoutComponent>
                    </Suspense>
                </> :
                exercise
            }
        </>
    );
});