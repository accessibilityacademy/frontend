import { FunctionComponent, useCallback, useRef, useState } from 'react';
import {
    Alert,
    AlertProps,
    AppBar,
    Box,
    Button,
    Divider,
    Grid,
    IconButton,
    Snackbar,
    SvgIcon,
    Tab,
    useMediaQuery,
    useTheme,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { Resizer } from '../../components/Resizer';
import { css } from '@emotion/react';
import { Previewer } from './Previewer/Previewer';
import { Exercise } from './Exercise/Exercise';
import { useExerciseService, useStore } from '../../services/ServiceProvider';
import { useData } from '../../helpers/useData';
import { useParams, useNavigate, useLocation } from 'react-router-dom';
import { AsyncHttpTask, HttpErrorType, isErrored, isLoaded, isLoading, StateType } from '../../helpers/AsyncHttpTask';
import { AnswerResult, Feedback } from '../../services/ExerciseService';
import { finalize } from 'rxjs/operators';
import { NotFoundPage } from '../NotFoundPage';
import { Header } from '../../components/Header';
import { TabContext, TabList } from '@material-ui/lab';
import { TabPanel } from '../../components/TabPanel';
import { StoreKey } from '../../services/StoreService';
import { Helmet } from 'react-helmet-async';
import { Editor, EmptyEditorContent } from './Editor/Editor';
import { useMemoCompare } from '../../helpers/useMemoCompare';
import { HttpStatus, Redirect } from '../../services/HttpContext';

const useStyles = (newLayout: boolean) => {
    const store = useStore();
    const exerciseStoredBasis = store.getValue(StoreKey.EXERCISE_RESIZER, 'number');
    const editorStoredBasis = store.getValue(StoreKey.EDITOR_PREVIEW_RESIZER, 'number');

    let exerciseBasis: string | null = null;
    let workplaceBasis: string | null = null;
    let editorBasis: string | null = null;
    let previewBasis: string | null = null;

    if(exerciseStoredBasis && exerciseStoredBasis <= 98 && exerciseStoredBasis >= 2) {
        exerciseBasis = exerciseStoredBasis + '%';
        workplaceBasis = 100 - exerciseStoredBasis + '%';
    }
    if(editorStoredBasis && editorStoredBasis <= 98 && editorStoredBasis >= 2) {
        editorBasis = editorStoredBasis + '%';
        previewBasis = 100 - editorStoredBasis + '%';
    }
    return {
        fullHeight: css({
            height: '100%',
        }),
        fullViewport: css({
            height: '100vh',
        }),
        zeroMinHeight: css({
            minHeight: 0,
            flexGrow: 1,
        }),
        exerciseContainer: css({
            flexShrink: 1,
            flexBasis: exerciseBasis ? exerciseBasis : newLayout ? '27.5%' : '35%',
            maxHeight: newLayout ? exerciseBasis ?? '27.5%' : undefined,
            maxWidth: newLayout ? undefined : exerciseBasis ?? '35%',
            position: 'relative',
            overflowX: 'visible',
            width: newLayout ? '100%' : undefined,
            minHeight: 0,
        }),
        workplaceContainer: css({
            overflowY: 'auto',
            flexShrink: 1,
            flexBasis: workplaceBasis ? workplaceBasis : newLayout ? '72.5%' : '65%',
            maxHeight: newLayout ? workplaceBasis ?? '72.5%' : undefined,
            maxWidth: newLayout ? undefined : workplaceBasis ?? '65%',
            width: newLayout ? '100%' : undefined,
            minWidth: 310, // prevent scroll on old layout
            minHeight: 250, // prevent scroll on new layout
        }),
        editorContainer: css({
            minHeight: 250,
            minWidth: 310,
            flexBasis: editorBasis ? editorBasis : newLayout ? '35%' : '50%',
            maxWidth: newLayout ? editorBasis ?? '35%' : undefined,
            maxHeight: newLayout ? undefined : editorBasis ?? '50%',
            position: 'relative',
            width: newLayout ? undefined : '100%',
        }),
        previewContainer: css({
            flexBasis: previewBasis ? previewBasis : newLayout ? '65%' : '50%',
            maxWidth: newLayout ? previewBasis ?? '65%' : undefined,
            maxHeight: newLayout ? undefined : previewBasis ?? '50%',
            minHeight: 250,
            minWidth: 215,
            width: newLayout ? undefined : '100%',
        }),
        noSelfScroll: css({
            '& .scrollable-container': {
                overflow: 'unset',
            },
        }),
        mobile: {
            panel: css({
                flexGrow: 1,
                padding: 0,
            }),
            tabBar: css({
                '@media screen and (min-height: 300px)': {
                    position: 'sticky',
                },
                top: 40, // compact header height
                position: 'static',
            }),
        },
    };
};

const titleCache: {
    [id: number]: string;
} = {};

interface ExercisePageProps {
    exercises?: number[];
    courseId?: number;
}

export const ExercisePage: FunctionComponent<ExercisePageProps> = (props) => {

    const newLayout = useStore().getValue(StoreKey.NEW_LAYOUT, 'boolean') ?? false;

    const params = useParams();
    const navigate = useNavigate();
    const location = useLocation();
    const exerciseService = useExerciseService();
    const [exercise, retryExerciseFetch] = useData('exercise', exerciseService.getExercise(Number(params.id)), [params.id]);

    const store = useStore();

    const slug = location.pathname.substr(location.pathname.indexOf('exercise')).split('/')[2];
    let redirect: JSX.Element | null = null;

    if(isLoaded(exercise)) {
        titleCache[exercise.data.id] = exercise.data.title;
    }
    if(isLoaded(exercise) && exercise.data.id === Number(params.id) && slug !== exercise.data.slug) {
        const walkthroughUrl = location.pathname.indexOf('walkthrough') !== -1 ? '/walkthrough' : '';
        redirect = <Redirect permanent to={exercise.data.slug + walkthroughUrl} replace />;
    }

    const styles = useStyles(newLayout);

    const [editorState, setEditorState] = useState(EmptyEditorContent);

    const onChange = useCallback((val: string) => {
        setEditorState(val);
    }, []);

    const nextQuestionUrl = useRef('');
    const onNext = useCallback(() => {
        navigate(nextQuestionUrl.current);
        setAlert(alert => ({ ...alert, open: false }));
        setAnswerResults(null);
        setSelectedPageMobile('exercise');
    }, [navigate]);

    const onReset = useCallback(() => {
        setAlert(alert => ({ ...alert, open: false }));
        setAnswerResults(null);
    }, []);

    const onSubmit = useCallback((val: string): Promise<void> => {
        if(exercise.type !== StateType.SUCCESS) return Promise.resolve();
        setAnswerResults(null);
        return new Promise(resolve => {
            AsyncHttpTask(exerciseService.submitAnswer(exercise.data.id, val)).pipe(finalize(resolve)).subscribe(results => {
                if(isErrored(results)) {
                    if(
                        results.errorType === HttpErrorType.SERVER_EXCEPTION ||
                        results.errorType === HttpErrorType.TIMEOUT ||
                        results.errorType === HttpErrorType.NETWORK_OUTAGE
                    ) {
                        setAlert({
                            open: true,
                            severity: 'error',
                            message: results.errorType === HttpErrorType.TIMEOUT ?
                                'Server timed out. Check network connectivity and try again later!' :
                                results.errorType === HttpErrorType.NETWORK_OUTAGE ?
                                    'No network connection. Please reconnect to the internet before retrying.' :
                                    'Server error',
                            actionLabel: 'Retry',
                            callback: () => {
                                onSubmit(val).catch(console.error);
                            },
                        });
                    } else if (results.error.response?.status === 400) {
                        setAlert({
                            open: true,
                            severity: 'warning',
                            message: 'Solution check timed out. Check that the answer does not contain an infinite loop and try again!',
                        });
                    }
                    return;
                }
                if(!isLoading(results)) {
                    if(results.data.requirementResults.some(requirement => !requirement.correct)) {
                        setSelectedPageMobile('exercise');
                        setAlert({
                            open: true,
                            severity: 'warning',
                            message: 'Oops! Your answer does not appear to satisfy all requirements. Try again.',
                        });
                    } else {
                        const completedExercisesSerialized = store.getValue(StoreKey.COMPLETED_EXERCISES, 'string');
                        let completedExercises: number[] | null = null;
                        if(completedExercisesSerialized) {
                            try {
                                completedExercises = JSON.parse(completedExercisesSerialized);
                            } catch (e) { }
                        }

                        if(completedExercises) {
                            if(!completedExercises.some(x => x === exercise.data.id)) {
                                completedExercises.push(exercise.data.id);
                            }
                        } else {
                            completedExercises = [exercise.data.id];
                        }
                        store.storeValue(StoreKey.COMPLETED_EXERCISES, JSON.stringify(completedExercises));

                        if(props.exercises) {
                            const currentExerciseIndex = props.exercises.indexOf(Number(params.id));
                            if(!props.exercises[currentExerciseIndex + 1]) {
                                nextQuestionUrl.current = '../../completed';
                                store.removeValue(StoreKey.CONTINUE_FROM);
                            } else {
                                const nextQuestionId = props.exercises[currentExerciseIndex + 1];
                                store.storeValue(
                                    StoreKey.CONTINUE_FROM,
                                    JSON.stringify({ cid: props.courseId, qid: nextQuestionId }),
                                    true,
                                );
                                nextQuestionUrl.current = '../' + nextQuestionId;
                            }
                        } else {
                            store.storeValue(StoreKey.CONTINUE_FROM, JSON.stringify({ qid: Number(params.id) + 1 }), true);
                            nextQuestionUrl.current = '../' + (Number(params.id) + 1);
                        }

                        setAlert({
                            open: true,
                            severity: 'success',
                            message: 'Congratulations. You are correct!',
                            actionLabel: 'Next',
                            callback: () => {
                                onNext();
                            },
                        });
                    }
                    setAnswerResults(results.data); // Set answer results after setAlert so that a screen reader first reads the alert
                }
            });
        });
    }, [exercise, exerciseService, onNext, params.id, props.courseId, props.exercises, store]);

    const onFeedbackSubmit = (feedback: Feedback): Promise<void> => {
        if(!isLoaded(exercise)) return Promise.resolve();
        return new Promise(resolve => {
            AsyncHttpTask(
                exerciseService.submitFeedback(exercise.data.id, editorState, feedback),
            ).pipe(finalize(resolve)).subscribe(results => {
                if(isErrored(results)) {
                    setAlert({
                        open: true,
                        severity: 'error',
                        message: 'Feedback submission failed. Try again later!',
                    });
                } else if(isLoaded(results)) {
                    setAlert({
                        open: true,
                        severity: 'success',
                        message: 'Feedback submitted',
                    });
                }
            });
        });
    };

    const [answerResults, setAnswerResults] = useState<AnswerResult | null>(null);

    const exerciseContainerRef = useRef<HTMLDivElement>(null!);
    const workplaceContainerRef = useRef<HTMLDivElement>(null!);
    const editorContainerRef = useRef<HTMLDivElement>(null!);
    const previewContainerRef = useRef<HTMLDivElement>(null!);

    const [alert, setAlert] = useState<{
        open: boolean;
        message: string;
        severity: AlertProps['severity'];
        actionLabel?: string;
        callback?: () => void;
    // eslint-disable-next-line @typescript-eslint/no-extra-parens
    }>({ open: false, message: '', severity: 'success' });


    let httpStatus: JSX.Element | null = null;
    if(isErrored(exercise)) {
        if(
            exercise.errorType === HttpErrorType.SERVER_EXCEPTION ||
            exercise.errorType === HttpErrorType.TIMEOUT ||
            exercise.errorType === HttpErrorType.NETWORK_OUTAGE
        ) {
            httpStatus = <HttpStatus code={exercise.error.response?.status ?? 500} />;
            if(!alert.open) {
                setAlert({
                    open: true,
                    severity: 'error',
                    message: exercise.errorType === HttpErrorType.TIMEOUT ?
                        'Server timed out. Check network connectivity and try again later!' :
                        exercise.errorType === HttpErrorType.NETWORK_OUTAGE ?
                            'No network connection. Please reconnect to the internet before retrying.' :
                            'Server error',
                    actionLabel: 'Retry',
                    callback: () => {
                        retryExerciseFetch();
                    },
                });
            }
        }
    }

    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('md'));
    const [selectedPageMobile, setSelectedPageMobile] = useState<'exercise' | 'editor' | 'preview'>('exercise');

    const verticalResizer = (
        <>
            <Divider orientation="vertical" flexItem />
            <SvgIcon viewBox="0 0 8 32" css={{ width: 4, height: 32, alignSelf: 'center', margin: '0 2px' }}>
                <rect width="30%" height="100%" x="0%" />
                <rect width="30%" height="100%" x="70%" />
            </SvgIcon>
            <Divider orientation="vertical" flexItem />
        </>
    );

    const horizontalResizer = (
        <>
            <Divider flexItem />
            <SvgIcon viewBox="0 0 32 8" css={{ width: 32, height: 4, alignSelf: 'center', margin: '2px 0' }}>
                <rect width="100%" height="30%" y="0%" />
                <rect width="100%" height="30%" y="70%" />
            </SvgIcon>
            <Divider flexItem />
        </>
    );

    const snackbar = (
        <Snackbar
            anchorOrigin={{ horizontal: 'center', vertical: 'bottom' }}
            open={alert.open}
        >
            <Alert
                severity={alert.severity}
                css={(theme) => ({
                    '& .MuiAlert-icon': {
                        fontSize: 27.5, // 22 * 1.25
                        alignItems: 'center',
                    },
                    '& .MuiAlert-message': {
                        display: 'flex',
                        alignItems: 'center',
                    },
                    '& .MuiAlert-action': {
                        paddingLeft: theme.spacing(2),
                        paddingTop: 0,
                    },
                    fontSize: '1.25rem',
                })}
                action={
                    alert.actionLabel ?
                        <Button sx={{ fontSize: '1.25rem' }} color="inherit" onClick={() => {
                            setAlert(alert => ({ ...alert, open: false }));
                            if(alert.callback) {
                                alert.callback();
                            }
                        }}>
                            {alert.actionLabel}
                        </Button> :
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            onClick={() => setAlert(alert => ({ ...alert, open: false }))}
                        >
                            <Close />
                        </IconButton>
                }
            >
                { alert.message }
            </Alert>
        </Snackbar>
    );


    let title: string | null = null;
    if (isErrored(exercise)) {
        title = 'Error';
    } else if (titleCache[Number(params.id)]) {
        title = titleCache[Number(params.id)];
    } else if (isLoaded(exercise)) {
        title = exercise.data.title;
    }

    let canonicalLink: JSX.Element | null = null;
    if(isLoaded(exercise) && props.courseId) {
        canonicalLink = <link rel="canonical" href={`/exercise/${exercise.data.id}/${exercise.data.slug}`} />;
    }

    const helmet = title &&
        <Helmet>
            <title>
                {title} - Accessibility Academy
            </title>
            {canonicalLink}
        </Helmet>;

    const mobile = useMemoCompare(
        <Box css={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
            {helmet}
            {snackbar}
            <TabContext value={selectedPageMobile}>
                <AppBar color="default" css={styles.mobile.tabBar}>
                    <TabList
                        onChange={(e, val) => setSelectedPageMobile(val)}
                        aria-label="TODO"
                        variant="fullWidth"
                        css={{ minHeight: '0 !important' }}
                    >
                        <Tab label="Exercise" value="exercise" />
                        <Tab label="Editor" value="editor" />
                        <Tab label="Preview" value="preview" />
                    </TabList>
                </AppBar>
                <TabPanel css={[styles.mobile.panel, styles.noSelfScroll]} value="exercise">
                    <Exercise mobile newLayout={false} exercise={exercise} results={answerResults} onFeedbackSubmit={onFeedbackSubmit} />
                </TabPanel>
                <TabPanel css={[styles.mobile.panel, styles.fullHeight]} value="editor">
                    <Editor
                        onSubmit={onSubmit}
                        onChange={onChange}
                        onNext={onNext}
                        onReset={onReset}
                        initialValue={isLoaded(exercise) ? exercise.data.code : ''}
                        value={editorState}
                        submitted={answerResults?.requirementResults.every(r => r.correct) ?? false}
                        compact
                    />
                </TabPanel>
                <TabPanel css={[styles.mobile.panel]} value="preview">
                    <Previewer
                        html={isLoaded(exercise) && editorState !== EmptyEditorContent ? exercise.data.hiddenCode + editorState : ''}
                    />
                </TabPanel>
            </TabContext>
        </Box>,
        () => !isMobile,
    );

    const desktop = useMemoCompare(
        <Grid container css={[styles.fullHeight, styles.zeroMinHeight]} direction={newLayout ? 'column' : 'row'} wrap="nowrap">
            {helmet}
            {snackbar}
            <Grid ref={exerciseContainerRef} item css={[styles.exerciseContainer, newLayout ? null : styles.fullHeight]}>
                <Exercise newLayout={newLayout} exercise={exercise} results={answerResults} onFeedbackSubmit={onFeedbackSubmit} />
                <Resizer
                    targetRef={exerciseContainerRef}
                    siblingRef={workplaceContainerRef}
                    direction={newLayout ? 'column' : 'row'}
                    storeChangeOnKey={StoreKey.EXERCISE_RESIZER}
                />
            </Grid>
            { newLayout ? horizontalResizer : verticalResizer }
            <Grid
                ref={workplaceContainerRef}
                item
                css={[styles.workplaceContainer, newLayout ? null : styles.fullHeight]}
                container
                direction={newLayout ? 'row' : 'column'}
                wrap="nowrap"
            >
                <Grid ref={editorContainerRef} item css={[styles.editorContainer, newLayout ? styles.fullHeight : null]}>
                    <Editor
                        onSubmit={onSubmit}
                        onChange={onChange}
                        onNext={onNext}
                        onReset={onReset}
                        initialValue={isLoaded(exercise) ? exercise.data.code : ''}
                        submitted={answerResults?.requirementResults.every(r => r.correct) ?? false}
                        value={editorState}
                        compact={false}
                    />
                    <Resizer
                        targetRef={editorContainerRef}
                        siblingRef={previewContainerRef}
                        direction={newLayout ? 'row' : 'column'}
                        storeChangeOnKey={StoreKey.EDITOR_PREVIEW_RESIZER}
                    />
                </Grid>
                { newLayout ? verticalResizer : horizontalResizer }
                <Grid ref={previewContainerRef} item css={[styles.previewContainer, newLayout ? styles.fullHeight : null]}>
                    <Previewer
                        html={isLoaded(exercise) && editorState !== EmptyEditorContent ? exercise.data.hiddenCode + editorState : ''}
                    />
                </Grid>
            </Grid>
        </Grid>,
        () => isMobile,
    );

    if(
        (isErrored(exercise) && exercise.errorType === HttpErrorType.CLIENT_EXCEPTION) ||
        (isLoaded(exercise) && props.exercises && !props.exercises.some(exerciseId => exerciseId === exercise.data.id))
    ) {
        return (
            <>
                <Header />
                <NotFoundPage />
            </>
        ) ;
    }

    return (
        <>
            {httpStatus}
            {redirect}
            <Box flexDirection="column" sx={{ display: { md: 'none', xs: 'flex' }, minHeight: '100vh' }}>
                <Header compact />
                <Box display="flex" alignItems="stretch" flexGrow={1}>
                    {mobile}
                </Box>
            </Box>
            <Box flexDirection="column" sx={{ display: { xs: 'none', md: 'flex' }, height: '100vh' }}>
                <Header compact />
                {desktop}
            </Box>
        </>
    );
};
