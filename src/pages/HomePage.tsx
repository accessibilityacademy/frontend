import {
    Button, Card, CardActionArea, CardContent, Container, Divider,
    LinearProgress, Skeleton, Typography, Link as MaterialLink, Box,
} from '@material-ui/core';
import React, { useState } from 'react';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Link } from 'react-router-dom';
import { CriticalError } from '../components/CriticalError';
import { HttpErrorType, isErrored, isLoaded, isLoading } from '../helpers/AsyncHttpTask';
import { useData } from '../helpers/useData';
import { useAxios, useCourseService, useExerciseService, useStore } from '../services/ServiceProvider';
import { StoreKey } from '../services/StoreService';
import { Header } from '../components/Header';

interface ContinueFromState {
    cid: number; // Course Id
}

export const HomePage = () => {

    const store = useStore();

    let continueFromState: ContinueFromState | null = null;
    const rawState = store.getValue(StoreKey.CONTINUE_FROM, 'string');
    if(rawState) {
        try {
            continueFromState = JSON.parse(rawState);
        } catch(e) { }
    }

    const courseService = useCourseService();
    const exerciseService = useExerciseService();

    interface ContinueFromData {
        url: string;
        title: string;
        courseTitle: string;
        completed: number;
        target: number;
    }

    let requiredRequest: Observable<ContinueFromData | null>;

    const [email, setEmail] = useState(store.getValue(StoreKey.EMAIL, 'string'));
    const [aem, setAem] = useState(store.getValue(StoreKey.AEM, 'string'));

    if(email) {
        requiredRequest = courseService
            .getCourse(4).pipe(switchMap((mandatoryCourse) => {
                if(mandatoryCourse.progress === mandatoryCourse.target) {
                    if(continueFromState?.cid) {
                        return courseService.getCourse(continueFromState.cid);
                    } else {
                        return of(null);
                    }
                } else {
                    return of(mandatoryCourse);
                }
            }))
            .pipe(switchMap((course) => {
                if(!course) return of(null);
                return exerciseService
                    .getExercise(course.exercises[course.progress])
                    .pipe(map((exercise) => ({
                        url: `/courses/${course.id}/exercise/${exercise.id}/${exercise.slug}`,
                        title: exercise.title,
                        courseTitle: course.title,
                        completed: course.progress,
                        target: course.target,
                    })));
            }));
    } else {
        requiredRequest = of(null);
    }


    const [continueFrom] = useData('continueFrom', requiredRequest, [email]);

    const axios = useAxios();
    const logout = () => {
        store.removeValue(StoreKey.EMAIL);
        store.removeValue(StoreKey.AEM);
        setEmail(null);
        setAem(null);
        axios.delete('/user').subscribe();
    };

    if(isErrored(continueFrom)) {
        if(continueFrom.errorType === HttpErrorType.CLIENT_EXCEPTION && continueFrom.error.response?.status === 401 && email) {
            setEmail(null);
            setAem(null);
            return null;
        }
        return <CriticalError error={continueFrom} />;
    }

    let continueFromCard: JSX.Element | null = null;

    if(isLoading(continueFrom) || continueFrom.data) {
        continueFromCard = (
            <Card>
                <CardActionArea
                    role="link"
                    aria-labelledby="continue"
                    component={Link}
                    to={isLoaded(continueFrom) ? continueFrom.data!.url : '#'}
                >
                    <CardContent>
                        <Typography id="continue" component="h1" color="textSecondary">
                            {isLoading(continueFrom) ?
                                <Skeleton /> :
                                continueFrom.data?.completed === 0 ?
                                    'Ξεκινήστε τις ασκήσεις':
                                    'Συνεχίστε από εκεί που σταματήσατε'
                            }
                        </Typography>
                        <Typography variant="h6" paragraph={isLoaded(continueFrom) && !continueFrom.data!.courseTitle}>
                            {isLoading(continueFrom) ?
                                <Skeleton variant="rectangular" height={32} sx={{ mb: 2 }} /> :
                                continueFrom.data?.title
                            }
                        </Typography>
                        {isLoading(continueFrom) ? null :
                            continueFrom.data?.courseTitle &&
                                <Typography variant="body2" paragraph>
                                    {continueFrom.data?.courseTitle}
                                </Typography>
                        }
                        <LinearProgress
                            variant={isLoading(continueFrom) ? 'indeterminate' : 'determinate'}
                            value={isLoaded(continueFrom) ? (continueFrom.data!.completed / continueFrom.data!.target) * 100 : undefined}
                        />
                        <Typography>
                            {isLoading(continueFrom) ?
                                <Skeleton variant="rectangular" height={24} width={64} sx={{ mt: 1 }} /> :
                                `${continueFrom.data!.completed} / ${continueFrom.data!.target}`
                            }
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        );
    }

    return (
        <>
            {email && <Header />}
            <Container component="main" css={{ paddingTop: 8 }}>
                <Typography variant="h6" component="h1">Καλωσήρθατε στο Accessibity Academy {email}</Typography>
                {!email &&
                    <>
                        <Typography>
                            Αν μπαίνετε για πρώτη φορά στον Ιστότοπο και δεν έχετε συμπληρώσει την φόρμα συναίνεσης για συμμετοχή, πατήστε:
                        </Typography>
                        <Button component={Link} to="/consent" variant="outlined">Συμμετοχή στην μελέτη</Button>

                        <Typography sx={{ mt: 3 }}>
                            Αν έχετε ήδη συμπληρώσει την φόρμα και θέλετε να ξανασυνδεθείτε για να συνεχίσετε την αξιολόγηση, πατήστε:
                        </Typography>
                        <Button component={Link} to="/continue" variant="outlined">Συνέχιση αξιολόγησης</Button>
                    </>
                }

                {email &&
                    <>
                        <Button variant="outlined" onClick={() => logout()}>Αποσύνδεση</Button>
                        <Typography sx={{ mt: 4 }} variant="h6" component="h1">Οδηγίες</Typography>
                        <Typography paragraph variant="body2">
                            Για την συμμετοχή σας στην μελέτη καλείστε να λύσετε κάποιες ασκήσεις διορθώνοντας τον δοσμένο κώδικα
                            έτσι ώστε να μην έχει προβλήματα προσβασιμότητας.
                            Ο χρόνος που θα χρειαστεί να αφιερώσεται είναι περίπου 3 με 4 ώρες. Δεν χρειάζεται να λύσετε όλα τα προβλήματα
                            με συνεχόμενη ροή.
                            {/* και εμφανίζεται στην αρχική σελίδα δηλαδή εκεί που βρίσκεστε τώρα. */}
                        </Typography>

                        <Divider />

                        <Typography mt={2} paragraph variant="body2">
                            Παρακαλούμε απαντήστε το παρακάτω quiz γνώσεων <strong>χωρίς</strong> να έχετε λύσει ασκήσεις στην πλατφόρμα
                            και <strong>χωρίς</strong> να αναζητήσετε κάτι στο διαδίκτυο.
                            Θα πρέπει να συμπληρώσετε το ίδιο email που δηλώσατε και στην φόρμα συναίνεσης "{email}". <br />
                            Σύνδεσμος: <br/>
                            <Button
                                variant="outlined"
                                href={'https://docs.google.com/forms/d/e/1FAIpQLSdl4ppHF6bbvuZi8k18BCaiH4rKm80m1yRs2_3Q81UvUH-8DA' +
                                    `/viewform?usp=pp_url&entry.1616529274=${email}&entry.2125484595=${aem ?? ''}`
                                }
                                rel="noopener"
                                target="_blank"
                            >
                                Quiz Γνώσεων
                            </Button>
                        </Typography>

                        <Divider />

                        <Typography mt={2} paragraph variant="body2">
                            Με την ολοκλήρωση του quiz γνώσεων μπορείτε πλέον να ξεκινήσετε να λύνετε τις ασκήσεις της εφαρμογής.
                            Οι ασκήσεις έχουν σχεδιαστεί με τέτοιο τρόπο ώστε να σας παροτρύνουν να ψάξετε για πιθανές λύσεις στα προβλήματα
                            χρησιμοποιώντας μηχανές αναζήτησης και άλλες γνωστές ιστοσελίδες documentation για το web όπως
                            το <MaterialLink href="https://developer.mozilla.org/en-US/" underline="always" rel="noopener">
                                MDN
                            </MaterialLink>.
                            Αυτό επιτυγχάνεται ελέγχοντας την λύση σας για τα προβλήματα προσβασιμότητας που αναγράφονται με γενικευμένο
                            τρόπο χωρίς να δίνονται συγκεκριμένες οδηγίες για το τι ακριβώς να αλλάξετε στον κώδικα. Φυσικά η κάθε άσκηση
                            έρχεται με έναν αριθμό από βοηθητικά links σε εξωτερικές πηγές οπού εξηγείται με λεπτομέρεια το πρόβλημα και οι
                            πιθανές λύσεις. Σε περίπτωση που έχετε κολλήσει σε μια άσκηση και δεν μπορείτε να βρείτε λύση υπάρχει το κουμπί:
                            "Show me the solution" το οποίο σας εξηγεί με λεπτομέρεια μια από τις λύσεις για να μπορέσετε να συνεχίσετε.
                            Προτείνεται να προσπαθήσετε πρώτα την άσκηση για ένα δεκάλεπτο πριν καταφύγετε στο να δείτε την λύση.
                            Η χρήση αυτού του κουμπιού σε συνδυασμό με τις εσφαλμένες υποβολές καταγράφεται ως μέρος της μελέτης
                            προκειμένου να αναγνωριστεί η δυσκολία των ασκήσεων, πιθανές βελτιώσεις στην εκφώνηση ή και στο ίδιο
                            το περιεχόμενο της άσκησης.
                        </Typography>
                        <Typography variant="body2">
                            Μερικά tips:
                        </Typography>
                        <ul css={{ marginTop: 0 }}>
                            <Typography variant="body2" component="li">
                                Μην διστάζετε να κάνετε υποβολή μια μη ολοκληρωμένη απάντηση.
                                Δεν μετράνε αρνητικά οι πολλές λανθασμένες υποβολές και δεν κοστίζουν τίποτα!
                                Με αυτό τον τρόπο μπορείτε να δείτε ποια υποερωτήματα έχετε λύσει με επιτυχία.
                            </Typography>
                            <Typography variant="body2" component="li">
                                Διαβάστε τις τεχνικές που αναγράφονται στο Learning Material, είναι πάντα σχετικές με το πρόβλημα.
                            </Typography>
                            <Typography variant="body2" component="li">
                                Κάποιες φορές είναι καλύτερο να ξεκινήσετε την προσπάθεια για την λύση μιας άσκησης ξανά από την αρχή.
                                Υπάρχει σχετικό reset κουμπί δίπλα στην υποβολή εκτός φυσικά και από το refresh του ίδιου του browser.
                            </Typography>
                        </ul>

                        <Box mb={2}>
                            { continueFromCard ??
                                <>
                                    <Typography variant="body2">
                                        Φαίνεται ότι τελειώσετε με τις συνιστώμενες ασκήσεις. Αν σας άρεσε η πλατφόρμα και θέλετε
                                        να λύσετε επιπλέον ασκήσεις μπορείτε να συνεχίσετε με εξτρά ασκήσεις. Διαφορετικά μπορείτε να
                                        συνεχίσετε στο επόμενο βήμα των οδηγιών.
                                    </Typography>
                                    <Button
                                        variant="outlined"
                                        component={Link}
                                        to="/courses/5"
                                        role="link"
                                    >
                                        Επιπλέον ασκήσεις
                                    </Button>
                                </>
                            }
                        </Box>

                        <Divider />

                        <Typography mt={2} paragraph variant="body2">
                            Με την ολοκλήρωση τουλάχιστον των συνιστώμενων ασκήσεων, ξανα συμπληρώστε το quiz γνώσεων για να αξιολογηθούν τα
                            μαθησιακά αποτελέσματα της πλατφόρμας. Οι ερωτήσεις είναι ίδιες, και πάλι παρακαλούμε να μην χρησιμοποιήσετε
                            εξωτερική βοήθεια όπως συζήτηση των απαντήσεων πριν το submit με φίλους, χρήση μηχανών αναζήτησης κτλ.
                            Θα πρέπει πάλι να προσέξετε να συμπληρώσετε το ίδιο email που δηλώσατε προηγουμένως για να γίνει η
                            αντιστοίχιση. Δεν βαθμολογείστε ούτε από την επίδοση σας στην εφαρμογή ούτε από την επίδοση σας στα Quiz.
                            <br />
                            Nέος σύνδεσμος: <br />
                            <Button
                                variant="outlined"
                                href={'https://docs.google.com/forms/d/e/1FAIpQLSe_KjzTFMlg0XM7PRFf31F0Dw14xgJpxZ2omsEu2L98M3PqUA' +
                                    `/viewform?usp=pp_url&entry.1616529274=${email}&entry.2125484595=${aem ?? ''}`
                                }
                                rel="noopener"
                                target="_blank"
                            >
                                Quiz Γνώσεων
                            </Button>
                        </Typography>

                        <Divider />

                        <Typography mt={2} paragraph variant="body2">
                            Ως τελευταίο βήμα συμπληρώστε το παρακάτω ερωτηματολόγιο αξιολόγησης της εκπαιδευτικής εμπειρίας και
                            της ευχρηστίας της πλατφόρμας. <br />
                            Σύνδεσμος: <br />
                            <Button
                                variant="outlined"
                                href={'https://docs.google.com/forms/d/e/1FAIpQLScO6wMu1vdfZ-2qAY5gr_AZujF6yOIIFa3lPPuVZCXUiqNZrg' +
                                `/viewform?usp=pp_url&entry.990144688=${email}&entry.1771053595=${aem ?? ''}`
                                }
                                rel="noopener"
                                target="_blank"
                            >
                                Ερωτηματολόγιο
                            </Button>
                        </Typography>

                        <Divider />

                        <Typography mt={2} paragraph variant="body2">
                            Για οποιοδήποτε τεχνικό πρόβλημα ή απορία μπορείτε να επικοινωνήσετε στο παρακάτω email: <br />
                            chatziem@csd.auth.gr
                        </Typography>

                        <Typography variant="body2" paragraph>
                            Ευχαριστώ για την συμμετοχή!
                        </Typography>
                    </>
                }
            </Container>
        </>
    );
};