import { Box, Container, Typography } from '@material-ui/core';
import { Helmet } from 'react-helmet-async';
import { HttpStatus } from '../services/HttpContext';

export const NotFoundPage = () => {
    return (
        <>
            <HttpStatus code={404} />
            <Helmet>
                <title>Not Found - Accessibility Academy</title>
            </Helmet>
            <Container>
                <Box textAlign="center">
                    <Typography variant="h2" component="h2">404</Typography>
                    <Typography variant="h4" component="h1">Page not available</Typography>
                    <Typography>
                        The page you requested was not found.
                        You either made a mistake when typing the address in the address bar or this page was removed.
                    </Typography>
                </Box>
            </Container>
        </>
    );
};