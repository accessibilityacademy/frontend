import { FunctionComponent, SVGProps } from 'react';
import { Card, CardActionArea, CardContent, CardMedia, Typography, useTheme } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { css } from '@emotion/react';


const useStyles = () => {
    const theme = useTheme();
    return {
        img: css({
            height: 250,
            width: '100%',
            padding: theme.spacing(4),
        }),
        title: css({
            textAlign: 'center',
        }),
        noPadding: css({
            padding: 0,
        }),
    };
};

interface PrincipleCardProps {
    title: string;
    image: FunctionComponent<SVGProps<SVGSVGElement> & {
        title?: string | undefined;
    }>;
    to: string;
    noPadding?: boolean;
};


export const PrincipleCard = (props: PrincipleCardProps) => {

    const styles = useStyles();
    // const theme = useTheme();

    return (
        <Card>
            <CardActionArea component={Link} to={props.to}>
                <CardMedia
                    component={props.image}
                    // stroke={theme.palette.text.primary}
                    // fill={theme.palette.text.primary}
                    css={[styles.img, props.noPadding ? styles.noPadding : null]}
                />
                <CardContent>
                    <Typography css={styles.title} variant="subtitle2">
                        {props.title}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};