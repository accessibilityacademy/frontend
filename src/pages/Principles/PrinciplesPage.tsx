import { Box, Container, Grid, Link as MaterialLink, Typography } from '@material-ui/core';
import { PrincipleCard } from './PrincipleCard';
import { Helmet } from 'react-helmet-async';

// import { ReactComponent as VisualImpairment } from '../../components/icons/visual-impairment.svg';
import { ReactComponent as Deaf } from '../../components/icons/deaf.svg';
import { ReactComponent as Brain } from '../../components/icons/brain-pink.svg';
// import { ReactComponent as KeyboardAndMouse } from '../../components/icons/keyboard-and-mouse.svg';
import { ReactComponent as Keyboard } from '../../components/icons/keyboard.svg';
import { ReactComponent as Responsive } from '../../components/icons/responsive.svg';
// import { ReactComponent as MultipleDeviceSupport } from '../../components/icons/multiple-device-support.svg';

export const PrinciplesPage = () => {
    return (
        <Container>
            <Helmet>
                <title>Principles - Accessibility Academy</title>
            </Helmet>
            <Typography variant="h4" component="h1" paragraph css={{ fontWeight: 500, textAlign: 'center' }}>Principles</Typography>
            <Grid container spacing={4}>
                <Grid item xs={12} sm={6} md={3}>
                    <PrincipleCard
                        title="Perceivability"
                        image={Deaf}
                        to="/exercise/1"
                        // noPadding={true}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <PrincipleCard
                        title="Operability"
                        image={Keyboard}
                        to="/exercise/16"
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <PrincipleCard
                        title="Understandability"
                        image={Brain}
                        to="/exercise/27"
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <PrincipleCard
                        title="Robustness"
                        image={Responsive}
                        to="/exercise/34"
                    />
                </Grid>
            </Grid>
            <Box>
                Icons made by
                <MaterialLink href="https://www.flaticon.com/authors/freepik" title="Freepik"> Freepik </MaterialLink>
                from
                <MaterialLink href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</MaterialLink>
            </Box>
        </Container>
    );
};