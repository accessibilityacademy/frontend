import { Outlet, Route, Routes as NativeRoutes } from 'react-router-dom';
import { HomePage } from './pages/HomePage';
import { ExercisePage } from './pages/Exercise/ΕxercisePage';
import { PrinciplesPage } from './pages/Principles/PrinciplesPage';
import { SettingsPage } from './pages/Settings/SettingsPage';
import { NotFoundPage } from './pages/NotFoundPage';
import { Header } from './components/Header';
import { Box, Container, Typography } from '@material-ui/core';
import { HttpStatus } from './services/HttpContext';
import { CourseHandler } from './pages/Exercise/CourseHandler';
import { CoursesPage } from './pages/Courses/CoursesPage';
import { BrowseExercisesPage } from './pages/BrowseExercises/BrowseExercisesPage';
import { ConsentPage } from './pages/ConsentPage';
import { ContinuePage } from './pages/ContinuePage';

export const Routes = () => {
    return (
        <NativeRoutes>
            <Route path="/courses/:id/*" element={<CourseHandler />} />
            <Route path="/exercise/:id/*" element={<ExercisePage />} />
            <Route path="/consent" element={<ConsentPage />} />
            <Route path="/continue" element={<ContinuePage />} />
            <Route path="/" element={<HomePage />} />
            <Route path="/" element={
                <>
                    <Header />
                    <main css={{ marginTop: 8 }}>
                        <Outlet />
                    </main>
                </>
            }>
                <Route path="/courses" element={<CoursesPage />} />
                <Route path="/browse-exercises" element={<BrowseExercisesPage />} />
                <Route path="/principles" element={<PrinciplesPage />} />
                <Route path="/settings" element={<SettingsPage />} />
                <Route path="/exercise-popup/*" element={
                    <Container>
                        <HttpStatus code={404} />
                        <Box textAlign="center">
                            <Typography variant="h4" component="h1">Page not available</Typography>
                            <Typography>
                                You probably reloaded the popup window of the exercise. Please close this window and reopen the popup!
                            </Typography>
                        </Box>
                    </Container>
                } />
                <Route path="*" element={<NotFoundPage />} />
            </Route>
        </NativeRoutes>
    );
};
