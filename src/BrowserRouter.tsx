import { useReducer, useLayoutEffect, createContext, useContext } from 'react';
import { BrowserHistory, createBrowserHistory, Update } from 'history';
import { BrowserRouterProps, Router } from 'react-router-dom';

let HistoryContext = createContext<BrowserHistory | null>(null);
if(typeof window !== 'undefined') {
    HistoryContext = createContext<BrowserHistory | null>(createBrowserHistory({ window }));
}

export const useHistory = () => {
    return useContext(HistoryContext);
};

export const BrowserRouter = ({ children }: BrowserRouterProps) => {

    const history = useHistory()!;

    const [state, dispatch] = useReducer(
        (_: Update, action: Update) => action,
        {
            action: history.action,
            location: history.location,
        },
    );

    useLayoutEffect(() => history.listen(dispatch), [history]);

    return (
        <Router
            children={children}
            action={state.action}
            location={state.location}
            navigator={history}
        />
    );
};